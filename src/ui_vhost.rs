
use crate::prelude::*;

use rocket::{Request, request::{FromRequest, Outcome}};
use rocket::http::uncased::UncasedStr;

const HEADER: &str = "Host";

/// Virtual hosts we expect
///
/// Each entry is a list of strings, being the expected vhosts.
///
/// `"*"` means any vhost is allowed
/// (and its presence renders any other values redundant).
#[derive(Deserialize, Debug, Deftly)]
#[derive_deftly(Guards)]
pub struct Vhosts {
    pub webhook: Vec<String>,
    pub ui: Vec<String>,
}

pub struct Is<S>(S);

/// Request guard, embodying a `Result` -- **unchecked**
///
/// Call `check`.
///
/// This is needed because a `FromRequest` impl cannot
/// readily generate a custom error document,
/// so instead we return the `WrongHost` to the route implementation.
pub struct ResultGuard<S>(Result<Is<S>, WrongVhost>);

impl<S> ResultGuard<S> {
    pub fn check(
        self,
        map_err: impl FnOnce(AE) -> WebError,
    ) -> Result<Is<S>, WE> {
        self.0.map_err(|e| map_err(e.0))
    }
}

#[derive(Error, Debug)]
#[error("{0}")]
pub struct WrongVhost(AE);

fn check_core(supplied: &UncasedStr, configured: &[String])
              -> Result<(), WrongVhost>
{
    if !configured.iter().any({
        |c| c == "*" || c == supplied}
    ) {
        return Err(WrongVhost(anyhow!(
 "wrong server name (vhost) requested: {HEADER:?} was {:?}, expected {}",
            supplied.as_str(), configured.join(" / "),
        )));
    }

    Ok(())
}

pub trait Selector: Default {
    fn configured(vhosts: &Vhosts) -> &[String];
}

#[async_trait]
impl<'r, S: Selector> FromRequest<'r> for ResultGuard<S> {
    type Error = String;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, String> {
        let supplied: &rocket::http::uri::Host = match {
            FromRequest::from_request(req).await
        } {
            Outcome::Success(y) => y,
            Outcome::Forward(x) => return Outcome::Forward(x),
            Outcome::Error((_s, i)) => match i {},
        };
        let gl = globals();
        let configured = S::configured(&gl.config.vhosts);
        let r = check_core(supplied.domain(), configured)
            .map(|()| Is(S::default()));
        Outcome::Success(ResultGuard(r))
    }
}

define_derive_deftly! {
    Guards:

    ${define V $<${pascal_case $fname}>}
    ${define S $<$V Selector>}

    $(
        /// Token indicating that the vhost is correct
        pub type $<Is $V> = Is<$S>;

        /// Request guard, embodying a `Result` - **unchecked**, call `check`
        pub type $<$V Result> = ResultGuard<$S>;

        #[derive(Default)]
        pub struct $S;

        impl Selector for $S {
            fn configured(vhosts: &Vhosts) -> &[String] {
                &vhosts.$fname
            }
        }
    )
}
use derive_deftly_template_Guards;
