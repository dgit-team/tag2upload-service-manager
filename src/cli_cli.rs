//! Real command-line program startup
#![cfg(not(test))]

use crate::prelude::*;

use clap::Parser;

/// Task that will send a systemd-style startup notification when we're up
///
/// start-stop-daemon can perform this protocol too,
/// and that makes for a more convenient way to daemonise
/// than messing about with double fork in Rust.
pub async fn sdnotify_task(started: oneshot::Receiver<Result<(), String>>) {
    use sd_notify::NotifyState as NS;
    let notify = |l| {
        let _: io::Result<()> = sd_notify::notify(true, l);
    };

    match async {
        let started = started.await
            .map_err(|_| format!("crashed during startup"))?
            .map_err(|e: String| format!("startup failed: {e}"))?;
        globals().await_running().await
            .map_err(|ShuttingDown| format!("shut down during startup?!"))?;
        Ok::<_, String>(started)
    }.await {
        Err(e) => notify(&[
            NS::Status(&e),
            NS::Errno(libc::EINVAL as _),
        ]),
        Ok(()) => notify(&[
            NS::Ready,
        ]),
    };
}

#[cfg(not(test))]
#[rocket::main]
pub async fn main_entrypoint() -> Result<(), AE> {
    use CliOperation as Op;

    let mut cli_options = CliOptions::parse();

    if cli_options.config.is_empty() {
        cli_options.config.push(format!(
            "{}/.t2usm.toml",
            std::env::var("HOME").context("need $HOME (or -c option)")?
        ));
    }        

    let whole_config = global::resolve_config(
        cli_options,
        Figment::new(),
    )?;

    match whole_config.cli_options.op.clone() {
        Op::RunManager {} => run_manager(whole_config).await,
    }
}

#[cfg(not(test))]
pub async fn run_manager(whole_config: WholeConfig) -> Result<(), AE> {
    let (sdnotify_started_tx, sdnotify_started_rx) = oneshot::channel();
    // We use tokio::spawn since we don't want this task to get involved
    // with our task tracking.  It is supposed to terminate, and isn't
    // likely to fail.
    tokio::spawn(sdnotify_task(sdnotify_started_rx));

    let started = global::startup(
        whole_config,
        Default::default(),
        Default::default(),
        |rocket| rocket,
    )
        .await;

    sdnotify_started_tx.send(
        started.as_ref()
            .map(|_: &global::Started| ())
            .map_err(|e: &StartupError| e.to_string())
    ).expect("notify task exited too soon!");

    started?
        .rocket
        .launch().await?;

    Ok(())
}
