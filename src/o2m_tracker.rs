
use crate::prelude::*;

use std::sync::{Mutex, MutexGuard};

#[derive(Debug, Default)]
pub struct WorkerTracker {
    state: Mutex<State>,
}

#[derive(Debug, Default)]
struct State {
    entries: Slab<WorkerReport>,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Deftly)]
#[derive_deftly(UiMap)]
pub struct WorkerReport {
    pub ident: String,
    pub last_contact: TimeT,
    pub phase: WorkerPhase,
    pub source: NoneIsEmpty<PackageName>,
    pub version: NoneIsEmpty<VersionString>,
    pub status: Option<JobStatus>,
    pub info: Option<String>,
}

#[derive(Debug, Clone, Ord, PartialOrd, Eq, PartialEq, Deftly)]
#[derive_deftly(UiDisplayEnum)]
pub enum WorkerPhase {
    Init,
    Idle,
    Selected,
    Building,
    Disconnected,
}

#[derive(Debug)]
pub struct TrackedWorker {
    tracker: Arc<WorkerTracker>,
    lid: usize,
}

impl WorkerTracker {
    pub fn new_worker(
        self: Arc<Self>,
        initial: WorkerReport,
    ) -> TrackedWorker {
        let mut state = self.lock();
        state.expire();

        let lid = state.entries.insert(initial);
        let tracker = self.clone();
        TrackedWorker { tracker, lid }
    }

    pub fn list_workers(&self) -> Vec<WorkerReport> {
        let mut list = {
            let mut state = self.lock();
            state.expire();
            state.entries.iter()
                .map(|(_, r)| r.clone())
                .collect_vec()
        };
        list.sort();
        list
    }

    fn lock(&self) -> MutexGuard<State> {
        self.state.lock().unwrap_or_else(|e| e.into_inner())
    }
}

impl WorkerReport {
    fn should_expire(&self, cutoff: TimeT) -> bool {
        match self.phase {
            WorkerPhase::Disconnected => self.last_contact < cutoff,
            _ => false,
        }
    }
}

impl TrackedWorker {
    pub fn update(&mut self, f: impl FnOnce(&mut WorkerReport)) {
        let mut state = self.tracker.lock();
        if let Some(ent) = state.entries.get_mut(self.lid) {
            f(ent)
        }
    }
}

impl State {
    fn expire(&mut self) {
        let gl = globals();
        let now = gl.now();
        let config = &gl.config;

        let Some(cutoff) = now
            .checked_sub(
                config.timeouts
                    .disconnected_worker_expire
                    .as_secs()
            )
        else { return };

        self.entries.retain(|_, ent| ! ent.should_expire(cutoff));
    }
}

impl Drop for TrackedWorker {
    fn drop(&mut self) {
        let now = globals().now();
        let mut state = self.tracker.lock();
        if let Some(ent) = state.entries.get_mut(self.lid) {
            ent.last_contact = now;
            ent.phase = WorkerPhase::Disconnected;
        }
    }
}

define_derive_deftly! {
    export UpdateWorkerReport:

    impl $ttype {
        pub fn update_worker_report(&self, wr: &mut WorkerReport) {
            $(
                ${when fmeta(worker_report)}
                wr.$fname = Some(self.$fname.clone()).into();
            )
        }
    }
}
pub use derive_deftly_template_UpdateWorkerReport;
