# tag2upload-service-manager

This is a component of Debian's
[tag2upload](https://wiki.debian.org/tag2upload)
project.

It is not intended for use elsewhere.

The latest version of this code is kept on
[salsa](https://salsa.debian.org/dgit-team/tag2upload-service-manager),
and may not have been uploaded to crates.io.  Uploading to crates.io
is not part of our deployment process.  The code is published on
crates.io mostly so that it can participate in crater runs.

Also, tags are not generally used.
The web service reports the commitid it was built from.
