CREATE TABLE IF NOT EXISTS jobs (
	jid		INTEGER PRIMARY KEY,
	-- JobData
	repo_git_url	TEXT NOT NULL,
	tag_name	TEXT NOT NULL,
	tag_objectid	TEXT NOT NULL,
	forge_host	TEXT NOT NULL,
	forge_namever	TEXT NOT NULL,
	forge_data	TEXT NOT NULL,
	source		TEXT NOT NULL,
	version		TEXT NOT NULL,
	--
	received	INTEGER NOT NULL,
	last_update	INTEGER NOT NULL,
	tag_data	TEXT NOT NULL,
	status		TEXT NOT NULL,
	duplicate_of	INTEGER, -- references, ish, jobs.jid
	processing	TEXT NOT NULL,
	info		TEXT NOT NULL
) STRICT;

CREATE TABLE IF NOT EXISTS pause_insns (
	pause_id	INTEGER PRIMARY KEY,
	pause_key	TEXT NOT NULL,
	pause_info	TEXT NOT NULL,
	throttle	INTEGER -- boolean
) STRICT;


-- Indices
--
-- In general, each of our indices is first on status,
-- which will allow easy filtering,
-- and cost very little if the status is not in the query.

CREATE INDEX IF NOT EXISTS jobs_st ON jobs (
	status
);

CREATE INDEX IF NOT EXISTS jobs_st_updated ON jobs (
	last_update
);

CREATE INDEX IF NOT EXISTS jobs_st_objectid ON jobs (
	status,
	tag_objectid
);

CREATE INDEX IF NOT EXISTS jobs_processing ON jobs (
	status
)
WHERE
	processing != ''
;
