
use super::*;

#[derive(Debug, Default)]
pub(super) struct BsqlUsed {
    locs: HashMap<CodeLocation, u32>,
    texts: BTreeSet<String>,
}

#[derive(Debug, Clone, Copy, Eq, PartialEq, Hash, Ord, PartialOrd)]
#[derive(derive_more::Display)]
#[display("{file}:{line}:{column}")]
pub struct CodeLocation {
    pub file: &'static str,
    pub line: u32,
    pub column: u32,
}
#[macro_export]
macro_rules! code_location { {} => { $crate::CodeLocation {
    file: file!(),
    line: line!(),
    column: column!(),
} } }

#[derive(Debug)]
pub struct CodeLocationAccumulator<'la>(MutexGuard<'la, BsqlUsed>);
impl CodeLocationAccumulator<'_> {
    pub fn push(&mut self, loc: CodeLocation) {
        trace!(%loc, "bsql! executed");
        *self.0.locs.entry(loc).or_default() += 1
    }
}

pub fn bsql_note_used(bsql: &BoundSql, sql_text: &str) {
    let gl = globals();
    let mut used = gl.test_suppl.bsql_used.lock().expect("poison?");
    used.texts.insert(sql_text.to_owned());
    bsql.bsql_note_locs(&mut CodeLocationAccumulator(used));
}

pub(super) fn t_check_all_bsql_used(tc: &TestCtx) -> TestResult<()> {
    // Sadly our regexp gives useless column information.
    // It's not so easy to do better.
    // The consequence is that if there were two bsql! on the same line
    // we'd miss one.
    // That's checked for below.

    let bsql_re = r#"\bbsql\s*!(?!\(\@)"#;
    let bsql_test_file_args = [
        "--",
        ":*.rs",
        ":!/src/test.rs",
        ":!/src/test",
    ];

    let mut cmd = std::process::Command::new("git");
    cmd.args(["grep", "--line-number", "-P",
              &format!(r#"^(?!\s*//).*{bsql_re}"#)]);
    cmd.args(bsql_test_file_args);

    let output = cmd.stdout_checked()?;

    let src_locs = output.split_terminator('\n').map(|l| {
        let loc = l.split_at(
            l.match_indices(':').nth(1).expect("too few colons").0
        ).0;

        debug!(loc, "found in source");
        loc
    }).collect::<BTreeSet<_>>();

    let used = tc.gl.test_suppl.bsql_used.lock().expect("poison?");
    let used_locs = used.locs.keys().map(
        |l| format!("{}:{}", l.file, l.line)
    )
        .collect::<HashSet<String>>();

    let untested_src_locs = src_locs.iter().filter_map(|loc| {
        if used_locs.contains(&**loc) { return None };
        error!(loc, "untested bsql!(...)");
        Some(())
    }).count();

    assert_eq!(untested_src_locs, 0);

    let mut cmd = std::process::Command::new("git");
    cmd.args(["grep", "--line-number", "-P",
              &format!(r#"{bsql_re}.*{bsql_re}"#)]);
    cmd.args(bsql_test_file_args);
    let output = cmd.output()?;
    trace!(wait_status=%output.status, "grep for doublw bsql");
    if output.status.success() {
        panic!(
            // See comment above, "Sadly our regexp".
            "double bsql found! **\n{}\n**\n\n",
            String::from_utf8_lossy(&output.stdout),
        );
    }
    assert_eq!(
        output.status.code(), Some(1),
        "stderr={}", String::from_utf8_lossy(&output.stderr),
    );

    Ok(())
}

pub(super) fn t_print_bsql_queries_executed(tc: &TestCtx) -> TestResult<()> {
    let used = tc.gl.test_suppl.bsql_used.lock().expect("poison?");

    let temp_db_file = format!("{}/empty-for-explain.db", tc.temp_dir());

    let sqlite_output = |qtxt: &_| {
        t_run_sqlite_batch_qtxt(&temp_db_file, qtxt)
    };

    sqlite_output(db_support::SCHEMA)?;

    let buffer = used.texts.iter().map(|q| {
        let plan = sqlite_output(&format!("EXPLAIN QUERY PLAN {q}"))?;
        TestResult::Ok((q, plan))
    }).collect::<TestResult<Vec<_>>>()?;

    let mut delim =
        "==================== sql query texts run ====================";
    for (q, plan) in buffer {
        println!("{delim}");
        println!("{q}");

        println!("----------");
        print!("{plan}");

        delim = "----------------------------------------"
    }
    println!("----------------------------------------");

    Ok(())
}
