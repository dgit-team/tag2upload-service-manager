
// This replaces Rocket's default subscriber.
//
// Ours is nicer, and also  prints messages from non-Rocket code.
// (I'm not sure what's wrong with the Rocket one
// but I'm not inclined to debug it.)

use crate::prelude::*;

pub use tracing_subscriber::filter::LevelFilter;
pub use tracing_subscriber::util::SubscriberInitExt as _;

pub use tracing_logrotate::{Interval, LevelConfig, ScheduleConfig};

pub fn default_schedule() -> ScheduleConfig {
    use LevelFilter as L;
    use Interval as I;

    [
        (L::DEBUG, I::Hour, 72),
        (L::INFO,  I::Day,  28),
    ]
        .into_iter()
        .map(|(level, interval, max_files)| {
            (level, LevelConfig { interval, max_files })
        })
        .collect::<BTreeMap<_, _>>()
        .into()
}

pub fn setup(config: &Config) -> Result<(), StartupError> {
    use StartupError as SE;

    #[cfg(test)]
    match crate::test::t_logging_setup() {
        Ok(()) => {},
        Err(()) => return Ok(()),
    }

    let level = config.log.level
        .or_else(|| {
            let _log_dir = config.log.dir.as_ref()?;
            let l = config.log.schedule.most_verbose_level();
            Some(l)
        })
        .unwrap_or(LevelFilter::INFO);

    let tracing = format!(
        "tag2upload_service_manager={},info,{}",
        level,
        config.log.tracing,
    );

    let filter = tracing_subscriber::filter::EnvFilter::builder()
        .parse(&tracing)
        .with_context(|| format!(
 "failed to parse tracing EnvFilter specification {tracing:?}"
        ))
        .map_err(SE::Logging)?;

    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_env_filter(filter);

    macro_rules! finish_with_subscriber { { $subscriber:expr } => {
        // tracing_subscribers' generics are quite fearsome.
        // the builder gets tainted by the writer.
        $subscriber
            .try_init()
            .map_err(|e| SE::Logging(
                anyhow!("tracing subscriber already initialised? {e}")
            ))?;
    } }

    if let Some(log_dir) = &config.log.dir {
        let writer = tracing_logrotate::Appender::new(
            log_dir.clone().into(),
            &config.log.schedule,
        )
            .context("failed to set up logfile writer")
            .map_err(SE::Logging)?;

        let subscriber = subscriber
            .with_ansi(false)
            .with_writer(writer);
        finish_with_subscriber!(subscriber);
    } else {
        finish_with_subscriber!(subscriber);
    };

    Ok(())
}
