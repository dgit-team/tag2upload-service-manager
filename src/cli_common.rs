//! Command-line handling code present even in tests (ie, when mocking)

use crate::prelude::*;

#[derive(Debug)]
pub struct WholeConfig {
    pub cli_options: CliOptions,
    pub config: Config,
    pub rocket_config: figment::Figment,
}

#[derive(Debug, clap::Parser)]
#[command(after_help = format!(
r#"tag2upload-service-manager ({})
Copyright Ian Jackson, Sean Whitton and contributors to tag2upload.
This is Free Software with NO WARRANTY.  GPL-3.0-or-later.
"#, crate::version::raw_info()))]
pub struct CliOptions {
    #[arg(short, long)]
    pub config: Vec<String>,

    #[arg(short = 'C', long)]
    pub config_toml: Vec<String>,

    #[command(subcommand)]
    pub op: CliOperation,
}

#[derive(Debug, Clone, clap::Subcommand)]
pub enum CliOperation {
    RunManager {},
}
