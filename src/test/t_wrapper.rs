//! Wrappers for async tests
//!
//! *Doesn't* handle lifecycle of actual manager instance etc.

use super::*;

macro_rules! run_async_test { {
    |$temp_dir:ident, $abort:ident| async move { $($body:tt)* }
} => {
    run_async_test(
        Arc::new(test_temp_dir!()),
        |$temp_dir: Arc<TestTempDir>, $abort: AbortHandle| async move {
            $($body)*
        }
    )
} }

/// Define a test function to run async, avoiding rightward drift
macro_rules! test_case { {
    fn $name:ident($temp_dir:ident, $abort:ident):
    $($body:tt)*
} => {
    #[test]
    #[traced_test]
    fn $name() {
        run_async_test!( |$temp_dir, $abort| async move { $($body)* } )
    }
} }

/// Wraps the main test future, performing various error handling etc.
///
///  * Provides the [`TestTempDir`] and ensure its proper lifetime
///  * Wraps the test future so as to obtain an `AbortHandle` 
///  * Imposes a backstop timeout so that hung tests eventually fail.
///  * Passes the `AbortHandle` *into* that future,
///    so that the InternalError machinery can abort the test,
///    if any of the tasks fail, etc.
///
/// `#[tokio::test]` makes it impossible to abort the test from within
/// one of its tasks.  Ideally we'd like
/// `#[tokio::test(unhandled_panic = "shutdown_runtime")]`
/// and then we could abort the test by panicking, but that's unstable.
pub(super) fn run_async_test<F, Fut>(temp_dir: Arc<TestTempDir>, f: F)
where
    F: FnOnce(Arc<TestTempDir>, AbortHandle) -> Fut + Send + 'static,
    Fut: Future<Output = TestResult<()>> + Send,
{
    let runtime = tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .expect("build runtime");

    // We have to make the task first, then obtain the abort handle,
    // and then we can call `f` to obtain the future.
    let (ah_tx, ah_rx) = oneshot::channel();

    let fut = {
        let temp_dir = temp_dir.clone();
        async {
            let abort = ah_rx.await.expect("AbortHandle not sent!");
            let fut = f(temp_dir, abort);
            fut.await
        }
    };

    let jh = runtime.spawn(fut);

    runtime.spawn({
        let abort = jh.abort_handle();
        async move {
            tokio::time::sleep(Duration::from_secs(59)).await;
            eprintln!("\n** test timeout! aborting! **\n");
            abort.abort();
        }
    });

    ah_tx.send(jh.abort_handle()).expect("AbortHandle send");

    match runtime.block_on(jh) {
        Ok(Ok(())) => {}
        Ok(Err(test_error)) => match test_error {}
        Err(je) => panic!("test aborted/paniced: {je:?} {je}"),
    }

    // Avoid deleting the temp dir any earlier
    drop::<Arc<TestTempDir>>(temp_dir);
}

