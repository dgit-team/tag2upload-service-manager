#![allow(clippy::style, clippy::complexity)]
#![deny(clippy::unwrap_used)]
#![deny(clippy::dbg_macro)]
#![allow(unstable_name_collisions)] // what a mess; rust-lang/rfcs!3240
#![allow(clippy::assigning_clones)] // .clone_into() is potentially confusing
#![doc=include_str!("../README.md")]

// Conventions:
//
// We use `T O D O` for things we may wish to change one day.
// We use `F I X M E` for things which should be fixed before deployment.

pub mod prelude;

#[macro_use]
pub mod error;
#[macro_use]
pub mod utils;
#[macro_use]
pub mod sql_types;
#[macro_use]
pub mod bsql_rows;
#[macro_use]
pub mod bsql_queries;
#[macro_use]
pub mod types_string_abstract;
#[macro_use]
pub mod t2umeta_abstract;
#[macro_use]
pub mod o2m_support;
#[macro_use]
pub mod ui_render;
#[macro_use]
pub mod ui_abstract;
#[macro_use]
pub mod global;

pub mod cli_common;
pub mod cli_cli;
pub mod config;
pub mod db_data;
pub mod db_support;
pub mod db_workflow;
pub mod dns;
pub mod expire;
pub mod fetcher;
pub mod fmt_cmp;
pub mod forge;
pub mod gitclone;
pub mod gitlab;
pub mod logging;
pub mod mini_sqlite_dump;
pub mod o2m_listener;
pub mod o2m_messages;
pub mod o2m_tracker;
pub mod t2umeta;
pub mod tracing_logrotate;
pub mod types;
pub mod ui_routes;
pub mod ui_vhost;
pub mod version;
pub mod webhook;

#[cfg(test)]
mod test;

#[doc(hidden)]
pub use derive_deftly;

#[cfg(not(test))]
mod test {
    #[derive(Default, Debug)]
    pub struct GlobalSupplement;

    #[derive(Default, Debug)]
    pub struct StateSupplement;

    #[derive(Debug, Clone, Copy)]
    pub struct CodeLocation {
        #[doc(hidden)]
        pub _hidden: (),
    }

    #[derive(Debug)]
    pub struct CodeLocationAccumulator<'la>(&'la mut void::Void);
    impl CodeLocationAccumulator<'_> {
        pub fn push(&mut self, _: CodeLocation) {
            void::unreachable(*self.0)
        }
    }

    #[macro_export]
    macro_rules! code_location { {} => {
        $crate::CodeLocation { _hidden: () }
    }}
}

pub use test::{CodeLocation, CodeLocationAccumulator};
