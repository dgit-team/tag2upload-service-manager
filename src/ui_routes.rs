
use crate::prelude::*;
use crate::ui_render::*;

fn status_is_queued() -> BoundSql<'static> {
    bsql!("    status IN (" [
                              JobStatus::Noticed,
                              JobStatus::Queued,
                              JobStatus::Building,
                            ] ")")
}

fn show_recent_threshold(gl: &Arc<Globals>) -> TimeT {
    let now = gl.now_systemtime();

    now
        .checked_sub(*gl.config.intervals.show_recent)
        .unwrap_or(now /* whatever */)
        .into()
}

struct FromInterestingJobs<'b> {
    queued_unrecent: BoundSql<'b>,
    anystatus_recent: BoundSql<'b>,
}

impl FromInterestingJobs<'_> {
    fn with<R>(recent: TimeT, f: impl FnOnce(FromInterestingJobs) -> R) -> R {
        f(FromInterestingJobs {
            queued_unrecent: bsql!("
                      FROM jobs
                     WHERE " (status_is_queued()) "
                       AND last_update < " recent "
            "),
            anystatus_recent: bsql!("
                      FROM jobs
                     WHERE last_update >= " recent "
            "),
        })
    }
}

define_derive_deftly! {
    RecursiveEnumIter:

    impl $ttype {
        fn iter() -> impl Iterator<Item = Self> {
            chain!( $(
                ${if v_is_unit {
                    [$vtype]
                } else {
                    $( $ftype::iter() ).map($vtype)
                }},
            ))
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq, Hash)]
#[derive(derive_more::Display, Deftly)]
#[derive_deftly(RecursiveEnumIter, UiDisplayEnum)]
enum ShownJobStatus {
    #[display("{_0:?}")]
    JobStatus(JobStatus),
    Duplicate,
}

impl Ord for ShownJobStatus {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        use ShownJobStatus as S;
        use JobStatus as J;
        let o = |v| match v {
            S::JobStatus(s @ J::Uploaded) => (30, Some(s)),
            S::Duplicate                  => (20, None),
            S::JobStatus(other)           => (10, Some(other)),
        };
        o(*self).cmp(&o(*other))
    }
}
impl PartialOrd for ShownJobStatus {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}

//---------- /hook/gitlab ----------

/// The path based in the public name of the kind of forge
///
/// This is baked into the configs of all the webhooks
#[rocket::post("/hook/gitlab", data="<payload>")]
async fn hook_gitlab(
    payload: RawSpecificWebhookPayload<gitlab::Payload>,
    vhost: ui_vhost::WebhookResult,
) -> Result<String, WebError> {
    payload.webhook_impl(vhost).await
}

//---------- / ----------

#[rocket::get("/")]
pub async fn r_toplevel(req_info: UiReqInfo) -> RenderedTemplate {

    let gl = globals();

    #[derive(Deftly, Ord, PartialOrd, Eq, PartialEq)]
    #[derive_deftly(FromSqlRow)]
    struct JobStatusCount {
        status: JobStatus,
        duplicate_of: Option<JobId>,
        count: i64,
    }

    #[derive(Deftly, Ord, PartialOrd, Eq, PartialEq)]
    #[derive_deftly(UiMap)]
    struct ShownJobStatusCount {
        status: ShownJobStatus,
        count: i64,
    }

    // Ideally we would list counts of *all* jobs, but sqlite can't maintain
    // and "index" giving count(*), and we don't want to do a full table scan.
    let recent = show_recent_threshold(&gl);

    let (status_count, pause) = FromInterestingJobs::with(recent, |from_jobs| {
        let (counts, pause) = db_transaction(TN::Readonly, |dbt| {
            
            let mut counts = ShownJobStatus::iter()
                .map(|s| (s, 0))
                .collect::<HashMap<_, i64>>();

            let mut add = |JobStatusCount { status, duplicate_of, count }| {
                let shown = if let Some(_) = duplicate_of {
                    ShownJobStatus::Duplicate
                } else {
                    ShownJobStatus::JobStatus(status)
                };
                *counts.entry(shown).or_default() += count;
                Ok::<_, IE>(())
            };

            // Even this way it's a bit tiresome.
            // sqlite won't optimise this the way we want,
            // unless we do it as two queries, - one of which scans
            // the whole queue.  Fine.

            dbt.bsql_query_n_call(
                bsql!("
                    SELECT status, duplicate_of, count(1) AS count
                         " (from_jobs.queued_unrecent) "
                  GROUP BY status, duplicate_of
                "),
                &mut add,
            )??;

            dbt.bsql_query_n_call(
                bsql!("
                    SELECT status, duplicate_of, 1 as count
                         " (from_jobs.anystatus_recent) "
                "),
                &mut add,
            )??;

            let pause = query_pause_insn_either(dbt)?;

            Ok::<_, IE>((counts, pause))
        })??;

        let mut counts = counts.into_iter()
            .map(|(status, count)| ShownJobStatusCount { status, count })
            .collect_vec();

        counts.sort();

        Ok::<_, IE>((UiSerializeRows(counts), pause))
    })?;

    let manager_status = match (&gl.state.borrow().shutdown_reason, &pause) {
        (None, None) => "running",
        (None, Some((_insn, None))) => "paused",
        (None, Some((_insn, Some(IsThrottled)))) => "throttled",
        (Some(Ok(ShuttingDown {})),_) => "shutting down",
        (Some(Err(_)),_) => "crashing",
    };

    let workers = {
        let w = gl.worker_tracker.list_workers();
        UiSerializeRows(w)
    };

    let mut n_workers_idle = 0;
    let mut n_workers_busy = 0;
    for w in &workers.0 {
        use WorkerPhase as WP;
        match w.phase {
            WP::Building | WP::Selected => n_workers_busy += 1,
            WP::Idle => n_workers_idle += 1,
            WP::Init | // doesn't count as connected
            WP::Disconnected => {}
        }
    }

    let recent = HtTimeT(recent);

    template_page! {
        "toplevel.html", req_info;
        {
            manager_status,
            pause_info: pause.map(|(insn, _thro)| insn.pause_info),
            status_count,
            recent,
            workers,
            n_workers_busy,
            n_workers_idle,
            n_workers_up: n_workers_busy + n_workers_idle,
        }
    }
}

//---------- /queue ----------

#[rocket::get("/queue")]
pub async fn r_queue(req_info: UiReqInfo) -> RenderedTemplate {
    let data = db_jobs_for_ui(
        status_is_queued(),
        bsql!(" jid ASC "),
    )?;

    template_page! {
        "queue.html", req_info;
        {
            jobs: data,
        }
    }
}

//---------- /recent ----------

#[rocket::get("/recent")]
pub async fn r_recent(req_info: UiReqInfo) -> RenderedTemplate {
    let gl = globals();
    let recent = show_recent_threshold(&gl);

    let jobs = FromInterestingJobs::with(recent, |from_jobs| {
        db_query_for_ui::<JobRow>(bsql!("
                          SELECT *
                         " (from_jobs.queued_unrecent) "
                          UNION
                          SELECT *
                         " (from_jobs.anystatus_recent) "
                        ORDER BY last_update DESC
        "))
    })?;

    let recent = HtTimeT(recent);
    let jobs = UiSerializeRows(jobs);

    template_page! {
        "recent.html", req_info;
        {
            jobs,
            recent,
        }
    }
}

//---------- /all-jobs ----------

#[rocket::get("/all-jobs")]
pub async fn r_all_jobs(req_info: UiReqInfo) -> RenderedTemplate {
    // TODO this query loads the whole db into memory
    // This is OK if there aren't too many.  If this becomes a problem
    // we should;
    //   - use watersheds to coalesce multiple queries for this route
    //   - do the db query and stream rendering rows to a temporary file
    //   - stream the temporary file out with pread

    let data = db_jobs_for_ui(
        bsql!(" TRUE "),
        bsql!(" last_update DESC "),
    )?;

    template_page! {
        "all-jobs.html", req_info;
        {
            jobs: data,
        }
    }
}

//---------- inventories ----------

load_template_parts! {
    "jobtable.part.html",
    "footer.part.html",
    "navbar.part.html",
    "recent-note.part.html",
}

pub(crate) const NAVBAR: &[NavbarEntry] = &[
    ("service",     "toplevel.html",     "/"),
    ("queue",       "queue.html",        "/queue"),
    ("recent",      "recent.html",       "/recent"),
    ("all jobs",    "all-jobs.html",     "/all-jobs"),
];

pub fn mount_all(rocket: RocketBuild) -> RocketBuild {
    rocket.mount("/", rocket::routes![
        hook_gitlab,
        r_toplevel,
        r_queue,
        r_recent,
        r_all_jobs,
    ])
}
