//! Miscellaneous test cases

use super::*;

#[test]
fn with_let_compiles() {
    let job_cond = bsql!("TRUE");

    with_let!([
        sql = bsql!(
        "
            UPDATE jobs
               SET status = " (JobStatus::Noticed) ",
                   duplicate = " (JobId::none()) ",
                   info = ''
             WHERE " (job_cond) "
               AND status in (" [ JobStatus::Noticed, JobStatus::Queued ] ")
        ");
    ], {
        let mut s = String::new();
        sql.bsql_extend_text(&mut s);
        eprintln!("{s}");
    });
}

test_case! {
    fn pages_json(temp_dir, abort):

    let tc = t_start(&temp_dir, abort, json!({})).await?;

    let resp = reqwest::Client::new()
        .get(format!("http://127.0.0.1:{}/", tc.port))
        .header("Host", VHOST_UI)
        .header("Accept", "application/json")
        .send()
        .await?
        .error_for_status()?
        .text()
        .await?;

    eprintln!("{resp}");

    let json: serde_json::Value = serde_json::from_str(&resp)?;
    let json = json.as_object().unwrap();
    assert_eq!(json.get("manager_status").unwrap().as_str().unwrap(),
               "running");
    assert_eq!(json.get("navbar"), None);

    Ok(())
}
