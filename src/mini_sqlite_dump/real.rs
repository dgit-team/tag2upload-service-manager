
use super::*;

pub fn write_real(mut w: impl io::Write, v: f64) -> Result<(), E> {
    if v == 0.0 && v.is_sign_positive() {
        write!(w, "0.")?;
    } else if v.is_normal() {
        write!(w, "{v:e}")?;
    } else if v.is_infinite() {
        // sqlite's reader saturates this to Inf
        write!(w, "{}e999", v.signum())?;
    } else {
        // NAN, or subnormal, or something.
        // Write the bits, since sqlite doesn't offer a faithful read syntax.
        //     https://sqlite.org/floatingpoint.html
        //     2.1.3. The ieee754_from_blob() and ieee754_to_blob() functions
        write!(w, "ieee754_from_blob(x'{:016x}')", v.to_bits())?;
    }
    Ok(())
}

#[cfg(test)]
pub fn interesting_f64s() -> Vec<f64> {
    // From WP Double-precision_floating-point_format
    const MIN_SUBNORM: f64 = 0.9406564584124654e-324;
    const MAX_SUBNORM: f64 = 2.2250738585072009e-308;
    let  snan:   f64 = f64::from_bits(0x7FF0_0000_0000_0001);

    // Inspired by the above.
    let qnan_pl: f64 = f64::from_bits(0x7FF8_abcd_0123_4567);

    vec![
        -f64::INFINITY, f64::MIN, -1., -f64::EPSILON, -f64::MIN_POSITIVE,
         f64::INFINITY, f64::MAX,  1.,  f64::EPSILON,  f64::MIN_POSITIVE,
         -0.0, -f64::NAN, -MIN_SUBNORM, -MAX_SUBNORM, -snan, -qnan_pl,
          0.0,  f64::NAN,  MIN_SUBNORM,  MAX_SUBNORM,  snan,  qnan_pl,
    ]
}
