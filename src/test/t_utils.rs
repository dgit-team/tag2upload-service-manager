//! Utilities for tests

use super::*;

pub fn from_temp<T>(
    temp_dir: &TestTempDir,
    f: impl FnOnce(&str) -> TestResult<T>,
) -> TestResult<TestTempDirGuard<T>> {
    let temp_dir = temp_dir.as_path_untracked();
    let temp_dir_str = temp_dir.to_str()
        .ok_or_else(|| anyhow!("non-utf-8 temp path"))?;
    let t = f(temp_dir_str)?;
    Ok(TestTempDirGuard::with_path(t, &temp_dir))
}

pub(super) fn get_job_maybe(jid: JobId) -> TestResult<Option<JobRow>> {
    db_transaction(TN::Readonly, |dbt| {
        let job: Option<JobRow> = dbt.bsql_query_01(bsql!(
            "SELECT * FROM jobs WHERE jid=" jid
        ))?;
        trace!(?job);
        TestResult::Ok(job)
    })?
}

pub(super) fn t_expect_job_deferring_shutdown(
    expect: Option<()>,
) -> TestResult<()> {
    let job = global::find_job_deferring_shutdown()?;
    trace!(?job, "deferring shutdown?");
    assert_eq!(job.map(|_: JobRow| ()), expect);
    Ok(())
}

pub async fn t_expect_wrong_vhost(
    what: &str,
    exp_status: reqwest::StatusCode,
    req: reqwest::RequestBuilder,
) -> TestResult<()> {
    let resp = req.send().await?;
    let status = resp.status();
    let message = resp.text().await?;
    trace!(%status, ?message, what, "(expected wrong vhost)");
    assert_eq!(status, exp_status);
    assert!(message.contains("wrong server name (vhost)"));
    Ok(())
}

/// Prefixes `f` with `./` if it doesn't already start with `./` or `/`
///
/// Useful for unquoting arguments for broken programs that don't do `--`,
/// such as `sqlite3(1)` in bookworm.
fn t_unquote_filename_bodge(f: &str) -> Cow<str> {
    if ['.','/'].iter().any(|c| f.starts_with(*c)) {
        Cow::Borrowed(f)
    } else {
        Cow::Owned(format!("./{f}"))
    }
}

fn t_run_sqlite_batch_general(
    db_file: &str,
    setup_cmd: impl FnOnce(&mut std::process::Command),
) -> TestResult<String> {
    let mut cmd = std::process::Command::new("sqlite3");
    cmd.args(["-batch", &t_unquote_filename_bodge(db_file)]);
    setup_cmd(&mut cmd);
    cmd.stdout_checked()
}

pub(super) fn t_run_sqlite_batch_qtxt(
    db_file: &str,
    sql_txt: &str,
) -> TestResult<String> {
    t_run_sqlite_batch_general(db_file, |cmd| { cmd.arg(sql_txt); })
}

pub(super) fn t_run_sqlite_batch_file(
    db_file: &str,
    sql_file: &str,
) -> TestResult<String> {
    let file = fs::File::open(sql_file)?;
    t_run_sqlite_batch_general(db_file, |cmd| { cmd.stdin(file); })
}

#[ext(CommandExt)]
pub(super) impl std::process::Command {
    fn stdout_checked(mut self) -> TestResult<String> {
        debug!(cmd=?self);

        let output = self.output().context("run")?;

        if !(output.status.success() && output.stderr.is_empty()) {
            let p = String::from_utf8_lossy(&output.stderr);
            eprintln!("*****\n{p}\n*****");
            panic!("command failed! wait status {}\n\n", output.status);
        }
        let s = String::from_utf8(output.stdout)?;
        Ok(s)
    }
}

impl From<TestError> for InternalError {
    fn from(te: TestError) -> InternalError {
        match te {}
    }
}
