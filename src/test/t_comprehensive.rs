//! Comprehensive test
//!
//! Supposed to exercise a complete lifecycle,
//! and all DB queries and all templates.

use super::*;

test_case! {
    fn comprehensive(temp_dir, abort):

    let mut tc = t_start(&temp_dir, abort, json!({})).await?;

    let block_fetch_start = tc.gl.t_block("fetch start");

    let port = tc.port;

    let test_repo_url =
        "https://salsa.debian.org/dgit-team/dgit-test-dummy.git";

    tc.map_url("https://salsa.debian.org/api",
                      format!("http://127.0.0.1:{port}/TEST/GITLAB/api"));
    tc.map_url(test_repo_url,
                      format!("file://{}", tc.dummy_repo));

    tc.load_page("/").await?;
    tc.load_page("/recent").await?;

    // test wrong vhosts

    t_expect_wrong_vhost(
        "webhook",
        reqwest::StatusCode::BAD_REQUEST,
        reqwest::Client::new()
            .post(format!("http://127.0.0.1:{port}/hook/gitlab"))
            .body(HOOK_BODY)
    ).await?;

    t_expect_wrong_vhost(
        "toiplevel",
        reqwest::StatusCode::NOT_FOUND,
        reqwest::Client::new()
            .get(format!("http://127.0.0.1:{port}/"))
    ).await?;

    // test data which gitlab sends when you use the "Test" button

    {
        let resp = reqwest::Client::new()
            .post(format!("http://127.0.0.1:{port}/hook/gitlab"))
            .header("Host", VHOST_WEBHOOK)
            .body(HOOK_BODY_UI_TEST)
            .send()
            .await?;
        let status = resp.status();
        let message = resp.text().await?;
        trace!(%status, ?message, "simulated gitlab UI test");
        assert_eq!(status, reqwest::StatusCode::BAD_REQUEST);
        assert!(message.contains(
            "seems like it might be properly configured"
        ));
    }

    // actually send a webhook:

    let resp = reqwest::Client::new()
        .post(format!("http://127.0.0.1:{port}/hook/gitlab"))
        .header("Host", VHOST_WEBHOOK)
        .body(HOOK_BODY)
        .send()
        .await?
        .error_for_status()?
        .text()
        .await?;

    trace!(?resp);

    let jid: JobId = regex!(r#"jid=(\d+)\b"#)
        .captures(&resp).ok_or("id not in response")?
        .get(1).ok_or("pattern broken?")?
        .as_str()
        .parse()?;

    trace!(?jid);

    let get_job_maybe = || get_job_maybe(jid);
    let get_job = || get_job_maybe().map(|j| j.expect("job didn't exist!"));

    let job = get_job()?;

    assert_eq!(job.jid, jid);
    assert_eq!(job.status, JobStatus::Noticed);

    tc.unblock(block_fetch_start);

    let job = tc.db_await_job(jid, |job| {
        let job = job?;
        (job.status != JobStatus::Noticed && job.processing.is_none())
            .then(|| job.clone())
    }).await?;

    assert!(job.info.contains("tag fetched, ready to process"),
            "{}", job.info);

    tc.load_page("/").await?;
    tc.load_page("/queue").await?;
    tc.load_page("/recent").await?;
    tc.load_page("/all-jobs").await?;

    let o2m_cl = UnixStream::connect(
        tc.o2m_path.clone()
    ).await?;

    tc.load_page("/").await?;

    let mut o2m_cl = Framed::new(o2m_cl, LinesCodec::new());

    assert_eq!(o2m_cl.next().await.unwrap()?, "t2u-manager-ready");
    o2m_cl.send("t2u-oracle-version 2").await?;
    o2m_cl.send("worker-id worker.example.org").await?;

    if let Some(reason) = &tc.gl.state.borrow().shutdown_reason {
        reason.as_ref().expect("internal error at some point during test");
    }
    let mut n_ayts = 0;
    let s = loop {
        t_expect_job_deferring_shutdown(None)?;

        let s = o2m_cl.next().await.unwrap()?;

        tc.load_page("/").await?;

        if s == "ayt" {
            n_ayts += 1;
            o2m_cl.send("ack").await?;
            continue;
        }
        if s.starts_with("job") {
            break s;
        }
        panic!("unexpected {s:?}");
    };

    let (_got_jobid, got_url) = s
        .strip_prefix("job ").unwrap()
        .split_once(' ').unwrap();
    assert_eq!(got_url, test_repo_url);
    t_expect_job_deferring_shutdown(Some(()))?;

    let tag_data = {
        use tokio_util::{codec::Decoder, bytes::BytesMut};

        let n_bytes: usize = o2m_cl.next().await.unwrap()?
            .strip_prefix("data-block ")
            .unwrap()
            .parse()?;

        struct ClumsyBytesCodec(usize);
        impl Decoder for ClumsyBytesCodec {
            type Item = String;
            type Error = anyhow::Error;
            fn decode(&mut self, src: &mut BytesMut)
                      -> Result<Option<String>, AE>
            {
                Ok(if src.len() < self.0 {
                    None
                } else {
                    let s = src.split_to(self.0).to_vec();
                    Some(String::from_utf8(s)?)
                })
            }
        }

        let mut special = o2m_cl.map_codec(|_| ClumsyBytesCodec(n_bytes));

        let tag_data = special.next().await.unwrap()?;

        o2m_cl = special.map_codec(|_| LinesCodec::new());

        tag_data
    };

    assert_eq!(o2m_cl.next().await.unwrap()?, "data-end");

    debug!("n_ayts={n_ayts} {s:?} l={}", tag_data.len());

    o2m_cl.send("message Tested OK!").await?;
    t_expect_job_deferring_shutdown(Some(()))?;
    o2m_cl.send("uploaded").await?;

    assert_eq!(o2m_cl.next().await.unwrap()?, "ayt");
    tc.load_page("/").await?;
    tc.load_page("/recent").await?;
    t_expect_job_deferring_shutdown(None)?;

    let job = get_job()?;
    assert_eq!(job.status, JobStatus::Uploaded);

    expire::expire_now(&tc.gl)?;
    let job = get_job()?;
    assert_eq!(job.status, JobStatus::Uploaded);

    tc.advance_time(tc.gl.config.intervals.expire.as_secs() as i64 + 1000);
    expire::expire_now(&tc.gl)?;
    assert!(get_job_maybe()?.is_none());
    tc.load_page("/").await?;

    {
        let archive_dir = tc.archive_dir.clone();
        let archive_file = glob::glob(&format!("{archive_dir}/*/*"))?
            .exactly_one()??
            .to_str().expect("UTF-8")
            .to_owned();

        assert!(
            glob::Pattern::new("*/[0-9]*[0-9]/expired*.dump.txt")?
                .matches(&archive_file),
            "{archive_file:?}",
        );

        let temp_db_file = 
            from_temp(&temp_dir, |t| Ok(format!("{t}/expired-reload.db")))?
            .into_untracked();
        t_run_sqlite_batch_file(&temp_db_file, &archive_file)?;

        let mut conn = rusqlite::Connection::open(&temp_db_file)?;
        let mut dbt = conn.transaction()?;
        let row: JobRow = dbt.bsql_query_1(bsql!("SELECT * FROM jobs"))?;
        assert_eq!(
            row.tag_data.as_ref().expect("NULL?!").to_string(),
            tag_data,
        );
    }

    // Finish up.  Final checks:
    // Check that nothing went wrong,
    // Check everything we want to test was indeed thoroughly tested.

    t_expect_job_deferring_shutdown(None)?;

    t_check_all_templates_tested(&tc)?;
    t_run_weblint(&tc)?;

    if match std::env::var("T2USM_COVERAGE_TESTS") {
        Ok(y) if y.starts_with('1') => true,
        Ok(y) if y.starts_with('0') => false,
        Err(std::env::VarError::NotPresent) => true, // enable by default
        other => panic!("cannot decide! {other:?}"),
    } {
        t_check_all_bsql_used(&tc)?;
    }

    t_print_bsql_queries_executed(&tc)?;

    drop(tc);

    Ok(())

}
