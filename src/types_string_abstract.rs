
use crate::prelude::*;

//---------- misc string macros ----------

#[macro_export]
macro_rules! define_string_newtypes { {
    $(
        $( # $attr:tt )*
        $name:ident;
    )*
} => {
    $(
        #[derive(Deftly)]
        $( # $attr )*
        #[derive(Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
        #[derive(Into, Deref, derive_more::Display)]
        #[derive(Serialize, Deserialize)]
        #[derive_deftly(SqlViaString, ParseViaTryFromString, DebugTransparent)]
        #[serde(try_from="String", into="String")]
        pub struct $name(String);

        impl $crate::ui_abstract::UiDisplay for $name {
            fn ui_display(&self) -> Cow<str> {
                Cow::Borrowed(&**self)
            }
        }
    )*
} }

define_derive_deftly! {
    export ParseViaTryFromString expect items:

    impl<$tgens> FromStr for $ttype {
        type Err = <$ttype as TryFrom<String>>::Error;
        fn from_str(s: &str) -> Result<Self, Self::Err> {
            s.to_owned().try_into()
        }
    }
}

//---------- define_standard_string_newtype ----------

/// Implement `TryFrom<String>` (via `ImplStandardStringNewtype`)
///
/// ### Input
///
/// ```
/// # use tag2upload_service_manager::{
/// #     prelude::*,
/// #     define_standard_string_newtype,
/// #     derive_deftly_template_SuitableForNoneIsEmpty,
/// #     derive_deftly_template_SqlViaString,
/// #     derive_deftly_template_ParseViaTryFromString,
/// #     derive_deftly_template_DebugTransparent,
/// # };
/// define_standard_string_newtype! {
///     TYPE [
///         error("ERROR MESSAGE CORE"); // optional
///     ]:
///     fn char_ok(c: char) -> bool {
///         todo!()
///     }
/// # #[cfg(any())]
/// # const _: &str = r#"
///     .. further items for ImplStandardStringNewtype ..
/// # "#;
/// }
/// ```
///
/// ### Generated
///
///  * `pub struct TYPE(String);` with many impls,
///    including [`SuitableForNoneIsEmpty`]
///    via [`define_string_newtypes!`].
///  * `impl TryFrom<String> for TYPE`.
///  * (`impl ImplStandardStringNewtype for TYPE`)
///
/// If `error` is provided, the macro will provide a suitable
/// `struct InvalidTYPE(AE)`,
/// make it the `Error` for the trait impls,
/// and provide implementation of `finish`.
///
/// Otherwise all the required items for `ImplStandardStringNewtype`
/// must be provided.
#[macro_export]
macro_rules! define_standard_string_newtype { {
    $( # $attr:tt )*
    $name:ident [
        $( error($error_msg:literal); )?
    ]:

    $($impls:tt)*
} => { $crate::prelude::paste! {

    $crate::define_string_newtypes! {
        $( # $attr )*
        #[derive_deftly(SuitableForNoneIsEmpty)]
        $name;
    }

    $(
        #[derive($crate::prelude::Error, Debug)]
        #[error("{}: {:#}", $error_msg, .0)]
        pub struct [< Invalid $name >]($crate::prelude::AE);
    )?

    impl $crate::types_string_abstract::ImplStandardStringNewtype for $name {
        $(
            // define the "repetition" count
            #[cfg(any(/* never */))] const _: &str = $error_msg;

            type Error = [< Invalid $name >];

            fn map_err(ae: AE) -> Self::Error {
                [< Invalid $name >](ae)
            }
        )?

        fn mk_value(s: String) -> Result<Self, AE> { Ok($name(s)) }

        $($impls)*
    }

    impl TryFrom<String> for $name {
        type Error =
 <Self as $crate::types_string_abstract::ImplStandardStringNewtype>::Error;
        fn try_from(s: String) -> Result<Self, Self::Error> {
            $crate::types_string_abstract::ImplStandardStringNewtype
                ::impl_try_from_string(s)
        }
    }

} } }

/// Helper trait for [`define_standard_string_newtype!`]
///
/// Don't name this trait.
/// But when you use the macro you must implement (some of) its items.
pub trait ImplStandardStringNewtype: Sized {
    /// The error type
    ///
    /// [`define_standard_string_newtype!`]'s
    /// `error()` feature can define this.
    type Error;

    /// Firstly, the input string is normalised with this
    fn normalise(s: String) -> Result<String, AE> { Ok(s) }

    /// All characters in the string must meet this test
    fn char_ok(c: char) -> bool;

    /// The initial character must additionally meet this test
    fn initial_char_ok(_c: char) -> bool { true }

    /// If the character set looks OK, these additional checks are made
    fn other_checks(_s: &str) -> Result<(), AE> { Ok(()) }

    /// Finally, the successful result is constructed or error converted
    ///
    /// Using [`define_standard_string_newtype!`]'s
    /// `error()` feature will define this.
    fn map_err(e: AE) -> Self::Error;

    /// Given a checked string, construct the actual `Self`
    ///
    /// [`define_standard_string_newtype!`]'s will always provide this.
    fn mk_value(s: String) -> Result<Self, AE>;

    /// Implementation detail
    ///
    /// Precisely the same as `TryFrom::try_from`.
    fn impl_try_from_string(s: String) -> Result<Self, Self::Error> {
        (move || {
            let s = Self::normalise(s)?;
            let c0 = s.chars().next().ok_or_else(|| anyhow!("empty"))?;
            if !Self::initial_char_ok(c0) {
                return Err(anyhow!("bad initial character {c0:?}"));
            }
            if let Some(bad) = s.chars().find(|c| !Self::char_ok(*c)) {
                return Err(anyhow!("bad character {bad:?}").into());
            }
            Self::other_checks(&s)?;
            let s = Self::mk_value(s)?;
            Ok(s)
        })()
            .map_err(Self::map_err)
    }
}

//---------- NoneIsEmpty ----------

define_derive_deftly! {
    /// See [`NoneIsEmpty`]
    export SuitableForNoneIsEmpty expect items:

    impl SuitableForNoneIsEmpty for $ttype {}

    #[test]
    fn ${snake_case SuitableForNoneIsEmpty_test_ $tname}() {
        let r: Result<$ttype, _> = String::new().try_into();
        r.expect_err("SuitableForNoneIsEmpty but can parse empty string");
    }
}

/// Wraps `Option`.  In sql, `None` is the empty string, not NULL.
#[derive(Clone, Debug, Hash, Eq, PartialEq, Ord, PartialOrd)]
#[derive(From, Into, Deref)]
#[derive(Deftly)]
#[derive(Serialize, Deserialize)]
#[derive_deftly(ParseViaTryFromString, SqlViaString)]
#[serde(try_from="String", into="String")]
pub struct NoneIsEmpty<V: SuitableForNoneIsEmpty>(pub Option<V>);

/// Various traits; also, promise that `""` isn't valid
///
/// Ideally, derive this with
/// [`SuitableForNoneIsEmpty`](crate::derive_deftly_template_SuitableForNoneIsEmpty),
/// which will produce a test case for `""` being unparseable.
pub trait SuitableForNoneIsEmpty:
    TryFrom<String, Error: std::error::Error + Send + Sync + 'static>
    + Into<String>
    + Clone
{}

impl<V: SuitableForNoneIsEmpty> TryFrom<String> for NoneIsEmpty<V> {
    type Error = V::Error;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        Ok(NoneIsEmpty(if s.is_empty() {
            None
        } else {
            Some(s.try_into()?)
        }))
    }
}
impl<V: SuitableForNoneIsEmpty> From<NoneIsEmpty<V>> for String {
    fn from(oe: NoneIsEmpty<V>) -> String {
        oe.0.map(|s| s.into()).unwrap_or_default()
    }
}

//---------- DersalizeViaFromStr ----------

define_derive_deftly! {
    DeserializeViaFromStr expect items:

    impl<'de, $tgens> Deserialize<'de> for $ttype<$tgens> {
        fn deserialize<D: Deserializer<'de>>(
            deser: D,
        ) -> Result<Self, D::Error> {
            let s: String = String::deserialize(deser)?;
            let r = s.parse()
                .map_err(|_| D::Error::invalid_value(
                    serde::de::Unexpected::Str(&s),
                    &${tmeta(deser(expect)) as str},
                ))?;
          ${if tmeta(deser(inner)) {
            let r = $tname { $( $fname: r ) };
          }}
            Ok(r)
        }
    }
}
