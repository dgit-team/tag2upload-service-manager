
use crate::prelude::*;

pub const SCHEMA: &str = include_str!("schema.sql");

pub type RowId = i64;

pub enum TransactionNature<'u> {
    Readonly,
    AcquireWork,
    Expiry,
    /// Update of a row, but we promise to `Update` later
    IntermediateUpdate,
    /// Insert or update of a row
    Update {
        tag_objectid: &'u GitObjectId,
        /// A job that we may update even though it's `processing`
        this_jid: Option<JobId>,
    },
}

thread_local! {
    static CONN: RefCell<Option<rusqlite::Connection>> = const {
        RefCell::new(None)
    }
}

fn with_conn_raw<R>(
    globals: &Arc<Globals>,
    f: impl FnOnce(Result<&mut rusqlite::Connection, rusqlite::Error>) -> R
) -> R {
    CONN.with(|tl| {
        let mut guard: std::cell::RefMut<_> = tl.borrow_mut();
        match guard.as_mut() {
            Some(y) => f(Ok(y)),
            None => {
                match rusqlite::Connection::open(&globals.config.files.db) {
                    Err(e) => return f(Err(e)),
                    Ok(y) => {
                        let conn = guard.insert(y);
                        f(Ok(conn))
                    },
                }
            },
        }            
    })
}

/// Runs `f` with a database transaction
///
/// If `f` returns `Ok`, we commit the transaction.
/// If the transaction cannot be committed, the `Ok(R)` is dropped.
pub fn db_transaction<R, E: From<InternalError>>(
    nature: TransactionNature,
    mut f: impl FnMut(&mut rusqlite::Transaction) -> Result<R, E>
) -> Result<Result<R, E>, InternalError> {
    let globals = globals();

    with_conn_raw(&globals, |dbconn| {
        let dbconn = dbconn
            .into_internal("reopen database connection")?;
        dbconn.with_transaction(
            match nature {
                TN::Readonly
                    => rusqlite::TransactionBehavior::Deferred,
                // With Immediate, we don't get BUSY errors later
                // in the actual transaction, so we don't need a retry loop.
                TN::IntermediateUpdate | TN::Update { .. } | TN::AcquireWork
                | TN::Expiry
                    => rusqlite::TransactionBehavior::Immediate,
            },
            |t| {
                let r = f(t);

                if r.is_ok() {

                    match nature {
                        TN::Readonly | TN::AcquireWork | TN::Expiry
                            | TN::IntermediateUpdate => {}
                        TN::Update { tag_objectid, this_jid } => {
                            db_workflow::coalesce_completed(
                                t, tag_objectid, this_jid,
                            )?;
                            globals.db_trigger.send_modify(
                                |_: &mut DbAssocState| {}
                            );
                        }
                    }

                }

                r
            }
        ).into_internal("manipulate db transaction")
    })
}

pub fn initialise(globals: &Arc<Globals>) -> Result<(), StartupError> {
    use StartupError as SE;

    with_conn_raw(globals, |dbconn| {
        let mut dbconn = dbconn.map_err(SE::DbOpen)?;


        // "execute" silently executes only the first:
        //   https://github.com/rusqlite/rusqlite/issues/397
        //   https://github.com/rusqlite/rusqlite/issues/1147
        dbconn.execute_batch(SCHEMA)
            .map_err(SE::ExecuteSchema)?;

        db_workflow::startup_reset(globals, &mut dbconn)?;

        Ok(())
    })
}
