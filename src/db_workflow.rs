
use crate::prelude::*;
use forge::SomeForge;

#[derive(Deref)]
pub struct JobInWorkflow {
    #[deref]
    job: JobRow,

    what: ProcessingInfo,
    log_xinfo: String,
}

pub struct Update<'u> {
    pub new_status: Option<JobStatus>,
    pub new_info: &'u str,
}

impl JobInWorkflow {
    /// Find a job to work on, checking for coalesce
    pub async fn start_for_forge(
        host: &Hostname,
        forge_namever: &str,
    ) -> Result<JobInWorkflow, QuitTask> {
        JobInWorkflow::start(
            // Coalesce bullet point 2.1.
            "fetch", format!("host={host}"),
            JobStatus::Noticed,
            // In theory we might find the same job as j2, so we must
            // eliminate that explicitly: j2 is supposed to have tag_data,
            // and with the current state of affairs, a job in Noticed
            // can't have tag_data.  But in the future we might find that
            // jobs in Noticed *do*, eg if gitlab starts providing us
            // with tag data.  We would then have the job in Noticed
            // while we verify the tag's objectid.
            bsql!(
              "    forge_host = " host "
               AND forge_namever = " forge_namever "
           AND NOT EXISTS ( SELECT 1 FROM jobs j2 WHERE (
                                  j2.tag_objectid = jobs.tag_objectid
                            AND   j2.jid != jobs.jid
                            AND   j2.status in (" [
                                JobStatus::Noticed,
                                JobStatus::Queued,
                                JobStatus::Building,
                                                  ] ")
                            AND   j2.tag_data != ''
                          ) )
            "),
            &db_workflow::Update {
                new_status: None,
                new_info: "fetching tag from forge"
            },
        ).await
    }

    /// Find a job to work on, matching some criteria
    ///
    /// # Cancellation safety
    ///
    /// This method is cancel safe.
    /// If the returned future is dropped before it completes,
    /// no job will be lost.
    pub async fn start(
        what: &str, log_xinfo: String,
        start_status: JobStatus,
        condition: BoundSql<'_>,
        update: &Update<'_>,
    ) -> Result<JobInWorkflow, QuitTask> {
        let what: ProcessingInfo = what.to_owned().try_into()
            .map_err(IE::from)?;

        let globals = globals();
        let mut subscription = globals.db_trigger.subscribe();

        let job = loop {
            test_hook(|| format!("{what} start")).await;

            globals.check_shutdown()?;

            trace!("[{log_xinfo}] {what}: looking for a job");

            let job = db_transaction(TN::AcquireWork, |dbt| {
                // Firstly, are we paused?
                if let Some(pause) = query_pause_insn(dbt, None)? {
                    trace!("[{log_xinfo}] {what}: paused {:?}",
                           &pause.pause_key);
                    globals.db_trigger.send_if_modified(|assoc| {
                        if assoc.paused.is_some() { return false };
                        info!(key=?pause.pause_key,
                              info=?pause.pause_info,
                              "pausing (possibly other insns too)");
                        assoc.paused = Some(IsPaused);
                        true
                    });
                    return Ok(None);
                }

                // Coalesce bullet point 1
                //
                // We can't the same job as j2, because we are looking
                // for jobs with processing='' and any j2 must have
                // processing!=''.
                let job: Option<JobRow> = dbt.bsql_query_01(bsql!("
                        SELECT * FROM jobs
                         WHERE " condition "
                           AND status = " start_status "
                           AND processing = ''
           AND NOT EXISTS ( SELECT 1 FROM jobs j2 WHERE (
                                  j2.processing != '' 
                            AND (
                                    jobs.repo_git_url == j2.repo_git_url 
                             OR     jobs.tag_objectid == j2.tag_objectid
                                )
                          ) )
                      ORDER BY jid
                         LIMIT 1
                    "),
                )?;

                let Some(job) = job
                else { return Ok(None) };

                let new_status = Self::update_inner(
                    Some(&what), &globals, dbt, &job,
                    update, &JobRowUpdate::default(),
                )?;

                Ok::<_, IE>(Some((job, new_status)))

            })??;

            if let Some((mut job, new_status)) = job {
                info!(jid=%job.jid, now=?new_status, info=?update.new_info,
                      "[{log_xinfo}] found ({what})");
                job.status = new_status;
                job.info = update.new_info.to_owned();

                break job;
            };

            trace!("[{log_xinfo}] {what}: waiting for a job");

            select! {
                biased;
                sd = globals.await_shutdown() => Err(sd)?,
                ch = subscription.changed() =>
                    ch.map_err(
                        |e| e.into_internal("await trigger")
                    )?,
            }
        };

        Ok(JobInWorkflow { job, what, log_xinfo })
    }

    pub fn forge_db_data<SF> (&self, forge: &SF) -> Result<SF::DbData, IE>
    where SF: SomeForge
    {
        if *self.data.forge_namever != forge.namever_str() {
            return Err(internal!(
                "called forge_db_data for {:?} on {:?}",
                forge.namever_str(), self.data.forge_namever,
            ));
        }
        let data = self.data.forge_data.as_raw_string().parse()
            .map_err(|e| internal!("parse forge_data {:?} for jid={}: {e}",
                                   self.data.forge_data, self.jid))?;
        Ok(data)
    }

    /// Caller must set `job.status` and `job.info`, iff transaction succeeds!
    ///
    /// (We can't do that here because the transaction might fail.)
    #[must_use]
    pub fn update_inner(
        what: Option<&ProcessingInfo>,
        globals: &Globals,
        dbt: &mut rusqlite::Transaction,
        job: &JobRow,
        update: &Update<'_>,
        set: &JobRowUpdate,
    ) -> Result<JobStatus, IE> {
        let jid = job.jid;
        let new_status = update.new_status.unwrap_or(job.status);

        let set = set.clone() | bsql_update! { JobRow {
            status: new_status,
            info: update.new_info.to_owned(),
            processing: NoneIsEmpty(what.cloned()),
            last_update: globals.now(),
        }};

        let set = set.bsql().ok_or_else(|| internal!("no update!"))?;

        dbt.bsql_exec_1_affected(bsql!("
                UPDATE jobs
                   SET " set "
                 WHERE jid = " jid "
                   AND status = " (job.status) "
        ")).into_internal("update job")?;

        Ok(new_status)
    }

    pub fn update(
        &mut self,
        update: &Update,
        set: &JobRowUpdate,
    ) -> Result<(), IE> {
        let JobInWorkflow { what, log_xinfo, .. } = &self;
        let globals = globals();

        let new_status = db_transaction(TN::IntermediateUpdate, |dbt| {
            let new_status = Self::update_inner(
                Some(&self.what), &globals, dbt, &self.job,
                update, set,
            )?;

            Ok::<_, IE>(new_status)
        })??;

        info!(jid=%self.jid, now=?new_status, info=?update.new_info,
              "[{log_xinfo}] updated ({what})");
        self.job.status = new_status;
        self.job.info = update.new_info.to_owned();

        Ok(())
    }
}

pub fn coalesce_completed(
    dbt: &mut rusqlite::Transaction,
    tag_objectid: &GitObjectId,
    this_jid: Option<JobId>,
) -> Result<(), IE> {
    #[derive(Deftly)]
    #[derive_deftly(FromSqlRow)]
    struct Earlier {
        jid: JobId,
        status: JobStatus,
    }
    with_let!([
        jobset_sql = bsql!("
                tag_objectid = " tag_objectid "
            AND ( processing = ''
             OR   jid = " this_jid " )
        ");
    ], {
        if let Some::<Earlier>(earlier) = dbt.bsql_query_01(bsql!("
                 SELECT jid, status
                   FROM jobs
                  WHERE " jobset_sql "
                    AND status in (" [
                        JobStatus::Uploaded,
                        JobStatus::Irrecoverable,
                    ]")
               ORDER BY jid ASC
                  LIMIT 1
           ")).into_internal("query for determining jobs for coalesce")?
        {
            bsql_update! {
                let update = JobRow {
                    status: earlier.status,
                    duplicate_of: Some(earlier.jid),
                    data: bsql_update! { JobData { }},
                    info: format!(""),
                }
            };

            dbt.bsql_exec(bsql!("
                    UPDATE jobs
                       SET " update "
                     WHERE " jobset_sql "
                       AND status in ("[
                           JobStatus::Noticed,
                           JobStatus::Queued,
                       ]")
            ")).into_internal("update dsuplicate jobs for coalesce")?;
        }
    });

    Ok(())
}

pub fn startup_reset(
    globals: &Arc<Globals>,
    dbconn: &mut rusqlite::Connection,
) -> Result<(), StartupError> {
    use StartupError as SE;

    bsql_update! {
        let reset_update = JobRow {
            processing: NoneIsEmpty(None),
        };
    };
    bsql_update! {
        let irrep_update = JobRow {
            status: JobStatus::Irrecoverable,
            last_update: globals.now(),
        };
    };

    let (n_cleared, n_irrep) = dbconn.with_transaction(
        rusqlite::TransactionBehavior::Immediate,
        |dbt| Ok::<_, StartupError>((

            dbt.bsql_exec(bsql!("
                UPDATE jobs
                   SET " reset_update "
                 WHERE processing != ''
            ")).into_internal("reset processing jobs")?,

            dbt.bsql_exec(bsql!("
                UPDATE jobs
                   SET " irrep_update ",
                       info = " (
 "service manager restarted while Building; may have been uploaded (was: "
                       ) " || info || " (")") "
                 WHERE processing != ''
            ")).into_internal("record irrepreable jobs")?,

        ))
    ).map_err(SE::DbAccess)??;

    if n_cleared != 0 {
        info!("cleared mark for {n_cleared} previously in-progress jobs");
    }
    if n_irrep != 0 {
        error!("marked {n_irrep} previously-Building jobs Irrecoverable");
    }

    Ok(())
}

pub fn query_pause_insn_either(
    dbt: &mut rusqlite::Transaction,
) -> Result<Option<(PauseInsn, Option<IsThrottled>)>, IE> {
    let row = dbt.bsql_query_01(bsql!("
        SELECT * FROM pause_insns
           ORDER BY throttle DESC
           LIMIT 1
    "))?;
    Ok(row.map(|row: PauseInsn| {
        let is_throttled = row.throttle.then_some(IsThrottled);
        (row, is_throttled)
    }))
}

fn query_pause_insn(
    dbt: &mut rusqlite::Transaction,
    only_throttle: Option<IsThrottled>,
) -> Result<Option<PauseInsn>, IE> {
    dbt.bsql_query_01(bsql!("
        SELECT * FROM pause_insns
           " (match only_throttle {
               None => bsql!(""),
               Some(IsThrottled) => bsql!(" WHERE throttle > 0 "),
             }) "
           LIMIT 1
    "))
}

pub fn check_not_throttled(
    dbt: &mut rusqlite::Transaction,
) -> Result<(), WebError> {
    match query_pause_insn(dbt, Some(IsThrottled))? {
        None => Ok(()),
        Some(pause) => Err(WebError::Throttled(pause.pause_info)),
    }
}

pub async fn unpause_task(gl: Arc<Globals>) -> TaskResult {
    let mut subscription = gl.db_trigger.subscribe();
    loop {
        subscription.wait_for(
            |assoc_state| assoc_state.paused.is_some()
        )
            .await
            .map_err(|_: watch::error::RecvError| ShuttingDown)?;

        tokio::time::sleep(*gl.config.timeouts.unpause_poll).await;

        db_transaction(TN::Readonly, |dbt| {
            if query_pause_insn(dbt, None)?.is_none() {
                info!("unpausing");
                gl.db_trigger.send_modify(|assoc_state| {
                    assoc_state.paused = None;
                });
            }
            Ok::<_, IE>(())
        })??;
    }
}

pub fn db_query_for_ui<R: FromSqlRow>(
    query: BoundSql<'_>,
) -> Result<Vec<R>, IE> {
    db_transaction(TN::Readonly, |dbt| {
        dbt.bsql_query_n_vec(query)
    })?
}

pub fn db_jobs_for_ui(
    condition: BoundSql<'_>,
    ordering: BoundSql<'_>,
) -> Result<impl Serialize, IE> {
    let data = db_query_for_ui::<JobRow>(bsql!("
            SELECT * FROM jobs
             WHERE " condition "
          ORDER BY " ordering "
        ")
    )?;

    Ok(UiSerializeRows(data))
}

impl Drop for JobInWorkflow {
    fn drop(&mut self) {
        bsql_update! {
            let update = JobRow {
                processing: NoneIsEmpty(None),
            }
        };

        db_transaction(TN::Update {
            this_jid: Some(self.jid),
            tag_objectid: &self.data.tag_objectid,
        }, |dbt| dbt.bsql_exec_1_affected(bsql!("
                        UPDATE jobs
                           SET " update "
                         WHERE jid = " (self.job.jid) "
                           AND processing = " (self.what)
        )))
            .unwrap_or_else(Err)
            .unwrap_or_else(|_: IE| { /* logged, what else can we do */ });
    }
}
