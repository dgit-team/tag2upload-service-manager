
use crate::prelude::*;

define_derive_deftly! {
    Outcome:

    impl Outcome {
        pub fn job_status(&self) -> JobStatus {
            match self { $(
                $vtype($vname {}) => JobStatus::$vname,
            ) }
        }
    }
}

#[derive(Deftly)]
#[derive_deftly(MessageToOracle)]
#[deftly(o2m(keyword = "t2u-manager-ready"))]
pub struct Ready {}

#[derive(Deftly, Debug)]
#[derive_deftly(MessageFromOracle)]
#[deftly(o2m(keyword = "t2u-oracle-version"))]
pub struct VersionRequest {
    pub version: u32,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
#[derive(strum::FromRepr)]
pub enum ProtocolVersion {
    // enums default to starting at 0; always define value explicitly
    PV2 = 2,
}

#[derive(Deftly, Debug)]
#[derive_deftly(MessageFromOracle)]
pub struct WorkerId {
    pub worker: types::WorkerId,
}

#[derive(Deftly)]
#[derive_deftly(MessageToOracle)]
pub struct Ayt {}

#[derive(Deftly)]
#[derive_deftly(MessageFromOracle)]
pub struct Ack {}

#[derive(Deftly)]
#[derive_deftly(MessageToOracle)]
pub struct Job {
    pub jid: JobId,
    pub url: String,
    #[deftly(o2m(data_blocks))]
    pub body: TagObjectData,
}

#[derive(Deftly)]
#[derive_deftly(MessageFromOracle)]
pub struct Message {
    #[deftly(o2m(rest_of_line))]
    pub message: String,
}

#[derive(Deftly)]
#[derive_deftly(MessageFromOracle)]
pub struct Uploaded {}

#[derive(Deftly)]
#[derive_deftly(MessageFromOracle)]
pub struct Irrecoverable {}

#[derive(Deftly)]
#[derive_deftly(MessageFromOracle, Outcome)]
pub enum Outcome {
    Uploaded(Uploaded),
    Irrecoverable(Irrecoverable),
}

#[derive(Error, Debug)]
#[derive(Deftly)]
#[derive_deftly(MessageToOracle, MessageFromOracle)]
#[deftly(o2m(infallible))]
#[error("{message}")]
pub struct ProtocolViolation {
    #[deftly(o2m(rest_of_line))]
    pub message: String,
}
