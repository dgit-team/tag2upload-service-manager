/// Like the `fmt-cmp` crate, but only handles comparisons to fixed strings

use crate::prelude::*;

struct State<'s> {
    remain: Result<&'s str, Differs>,
}

#[derive(Copy, Clone)]
struct Differs;

impl From<Differs> for fmt::Error {
    fn from(Differs: Differs) -> fmt::Error { fmt::Error }
}

impl fmt::Write for State<'_> {
    fn write_str(&mut self, chunk: &str) -> fmt::Result {
        self.remain = self.remain?.strip_prefix(chunk).ok_or(Differs);
        self.remain?;
        Ok(())
    }
}

pub fn display_eq(s: &str, t: impl Display) -> bool {
    let mut state = State { remain: Ok(s) };
    match (write!(&mut state, "{}", t), state.remain) {
        (Ok(()), Ok(remain)) => remain.is_empty(),
        (_, Err(Differs)) => false,
        (Err(fmt::Error), Ok(_)) => panic!("display impl panicked"),
    }
}

#[test]
fn test() {
    struct DisplayStrs<'s>(&'s [&'s str]);
    impl Display for DisplayStrs<'_> {
        fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
            for s in self.0 {
                Display::fmt(s, f)?;
            }
            Ok(())
        }
    }

    let chk = |eq, s, l| {
        assert_eq!(
            eq,
            display_eq(s, DisplayStrs(l)),
            "{s:?} {l:?}",
        );
    };

    chk(true, "abc", &["abc"]);
    chk(true, "abc", &["a", "bc"]);
    chk(true, "abc", &["a", "", "bc"]);
    chk(true, "abc", &["a", "", "bc", ""]);

    chk(false, "abc", &["ab"]);
    chk(false, "abc", &["a", "b"]);
    chk(false, "abc", &["", "a", "", "b", ""]);

    chk(false, "abc", &["abcd"]);
    chk(false, "abc", &["ab", "cd"]);
    chk(false, "abc", &["ab", "c", "d"]);
    chk(false, "abc", &["ab", "c", "", "d"]);

    chk(false, "abc", &["ab2"]);
    chk(false, "abc", &["ab", "2"]);
}
