//! forge API
//!
//! To add a forge
//!
//!  * add an implementation of `SomeForge` to `FORGES`
//!  * define an implementation of `SomeWebhookPayload`
//!  * add the route to `routes.rs`,
//!    taking `RawSpecificWebhookPayload<'_, your::Payload>`,
//!    and dispatching to `RawSpecificWebhookPayload::webhook_impl`.
//!
//! `gitlab.rs` has an example; its route is in `routes.rs`.

use crate::prelude::*;

#[async_trait]
pub trait SomeForge: Debug + Send + Sync + 'static {
    type DbData: Display + FromStr<Err: Display> where Self: Sized;

    /// Matches [`config::Forge::kind`]
    ///
    /// Also `SomeWebhookPayload::KIND_NAME`.
    fn kind_name(&self) -> &'static str { "gitlab" }

    /// Name and recorded data version
    ///
    /// This allows us to change the format of the data in the db,
    /// especially in the `forge_data` field.
    ///
    /// For gitlab this is `gitlab-1`
    fn namever_str(&self) -> &str;

    /// Try to fetches some tag data
    ///
    /// Should find a job with [`JobInWorkflow::start`]
    /// and process it.
    /// Should call `job.update` to report intermediate progress.
    /// and, eventually `record_fetch_outcome`.
    ///
    /// `tmpdir` is for this function's exclusive use,
    /// until it returns.
    async fn make_progress(
        &self,
        host: &Hostname,
        task_tmpdir: &str,
    ) -> Result<(), QuitTask>;
}

pub const FORGES: &[&dyn SomeForge] = &[&gitlab::Forge1];
