#![allow(clippy::unwrap_used)]

#[macro_use]
mod t_utils;
#[macro_use]
mod t_wrapper;
#[macro_use]
mod t_bsql;
mod t_block;
mod t_data;
mod t_tera;
mod t_test_ctx;
mod t_url_map;
use t_block::*;
pub use t_bsql::*;
pub use t_data::*;
use t_tera::*;
use t_test_ctx::*;
pub use t_url_map::*;
use t_utils::*;
use t_wrapper::*;

mod t_comprehensive;
mod t_jcoalesce;
mod t_misc;

use crate::prelude::*;
use test_prelude::*;

//---------- data structures ----------

#[derive(Debug)]
pub struct GlobalSupplement {
    url_map: UrlMap,

    templates_rendered_recently: TemplatesRenderedRecently,
    temp_dir: Arc<TestTempDir>,

    /// Added to all times
    ///
    /// In addition to `config::Testing.time_offset`.
    pub simulated_time_advance: Mutex<i64>,

    bsql_used: Mutex<BsqlUsed>,
}

#[derive(Debug)]
pub struct StateSupplement {
    blocks: BlockBlocks,
    abort: AbortHandle,

    // For lifetime.
    // In StateSupplement since that gets dropped *after* GlobalSupplement.
    #[allow(dead_code)]
    temp_dir: Arc<TestTempDir>,
}

//---------- implementations ----------

impl GlobalSupplement {
    fn new(temp_dir: Arc<TestTempDir>) -> Self {
        GlobalSupplement {
            simulated_time_advance: Default::default(),
            url_map: Default::default(),
            temp_dir,
            templates_rendered_recently: Default::default(),
            bsql_used: Default::default(),
        }
    }
}

impl Drop for StateSupplement {
    fn drop(&mut self) {
        trace!("<!-- dropping StateSupplement -->");
    }
}

pub async fn hook_point(point: String) {
    let gl = globals();
    gl.t_hook_blocks(&point).await;
}

impl Globals {
    pub fn t_shutdown_handlers(&self) -> Result<(), ()> {
        Err(())
    }
}

impl TestCtx {
    fn startup(&self, initial_block: BlockHook) -> TestResult<()> {
        self.report_time("startup");
        fs::create_dir(format!("{}/rendered", self.temp_dir()))?;
        self.unblock(initial_block);
        Ok(())
    }
    fn report_time(&self, when: impl Display) {
        let now = humantime::Timestamp::from(self.gl.now_systemtime());
        trace!(%now, %when, "simulated time");
    }

    fn advance_time(&self, advance: i64) {
        self.report_time("before advance");
        *self.gl.test_suppl.simulated_time_advance.lock().unwrap()
            += advance;
        self.report_time("after advance");
    }
}

pub fn t_logging_setup() -> Result<(), ()> {
    return Err(());
}
