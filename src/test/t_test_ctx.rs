
use super::*;

pub struct TestCtx {
    pub gl: Arc<Globals>,
    pub port: u16,
    pub templates_rendered: TemplatesRendered,

    pub dummy_repo: String,
    pub o2m_path: String,
    pub archive_dir: String,
}

pub(super) async fn t_start(
    temp_dir: &Arc<TestTempDir>,
    abort: AbortHandle,
    extra_config: serde_json::Value,
) -> TestResult<TestCtx> {
    let db_path = temp_dir.used_by(|path| {
        format!("{}/t2usm.db", path.to_str().expect("temp dir not utf-8"))
    });
    // We have a global db connection pool, in thread-local variables.
    // Every #[test] test runs in a separate thread
    // (test_temp_dir relies on this too)
    // so the "global" db connections won't overlap between tests.
    //
    // There is in theory a risk that the db connection might be
    // used on a some thread that outlives the temp_dir,
    // But, nothing creates threads other than the tokio runtime,
    // and we're using the single threaded one.

    let scratch_dir_path = from_temp(&temp_dir, |path| {
        let path = format!("{}/scratch", path);
        fs::create_dir(&path)?;
        Ok::<_, TestError>(path)
    })?;

    let o2m_path = from_temp(&temp_dir, |p| Ok(format!("{p}/o2m")))?;
    let port_report_file = from_temp(&temp_dir, |p| Ok(format!("{p}/port")))?;
    let archive_dir = from_temp(&temp_dir, |p| Ok(format!("{p}/archive")))?;

    fs::create_dir(archive_dir.clone().into_untracked())?;

    let simulated_time_base_offset = -(
        SystemTime::now()
            .duration_since(
                humantime::parse_rfc3339(SIMULATED_START_TIME)
                    .expect("parse SIMULATED_START_TIME")
            )?
            .as_secs()
            as i64
    );

    let base_config = Figment::new()
        .merge(figment::providers::Serialized::globals(json! {{
            "files": {
                // untracked is OK, because we feed the Arc to Rocket
                // to manage as part of its state,
                // so it will outlive the Rocket
                "db": db_path.clone().into_untracked(),
                "scratch_dir": scratch_dir_path.into_untracked(),
                "archive_dir": archive_dir.clone().into_untracked(),
                "o2m_socket": o2m_path.clone().into_untracked(),
                "port_report_file": port_report_file.clone().into_untracked(),
            },
            "t2u": {
                "distro": "debian",
                // lifetime: tmp is used only once we have GLOBALS,
                // by which time a clone of the original it's in GlobalHooks
                "forges": [{
                    "kind": "gitlab",
                    "host": FORGE_HOST,
                    "allow": ["127.0.0.1/32"],
                }],
            },
            "vhosts": {
                "ui": [VHOST_UI],
                "webhook": [VHOST_WEBHOOK],
            },
            "testing": {
                "time_offset": simulated_time_base_offset,
            },
            "rocket": {
                "port": 0,
                "cli_colors": false,
            },
        }}))
        .merge(figment::providers::Serialized::globals(extra_config));

    let dummy_repo = from_temp(&temp_dir, |path| {
        let tarfile = std::fs::File::open(DUMMY_REPO_TARBALL)
            .context(DUMMY_REPO_TARBALL)?;
        let std::process::Output { stdout: _, stderr, status } =
            std::process::Command::new("tar")
                .current_dir(&path)
                // CI runs as "root" (in the container namespace),
                // which makes tar (by default) extract ownerships
                // which makes git complain the files have unsafe ownership.
                .args(["--no-same-owner", "-Jx"])
                .stdin(tarfile)
                .output().context("spawn tar")?;
        if !stderr.is_empty() {
            debug!("tar printed:\n{}", String::from_utf8_lossy(&stderr));
        }
        if !status.success() {
            return Err(anyhow!("tar failed: {status}").into());
        }
        let repo = format!("{path}/dgit-test-dummy.git");
        let repo = {
            std::path::absolute(repo)
                .context("make path abs")?
                .to_str().ok_or_else(|| anyhow!("non-utf8 cwd"))?
                .to_owned()
        };
        Ok(repo)
    })?;

    let cli_options = CliOptions {
        config: vec![],
        config_toml: vec![],
        op: CliOperation::RunManager {},
    };

    let rocket_hook = {
        let temp_dir = temp_dir.clone();

        move |rocket: RocketBuild| {

            // Make sure temp_dir outlives rocket, see above
            let rocket = rocket.manage(temp_dir);

            let rocket = rocket.mount("/TEST/GITLAB", rocket::routes![
                r_mock_gitlab_tag_info,
            ]);

            rocket
        }
    };

    let (blocks, initial_block) = BlockBlocks::new_all();

    let state_suppl = StateSupplement {
        blocks,
        temp_dir: temp_dir.clone(),
        abort: abort.clone(),
    };

    let whole_config = crate::global::resolve_config(
        cli_options,
        base_config,
    )?;
    let started = crate::global::startup(
        whole_config,
        GlobalSupplement::new(temp_dir.clone()),
        state_suppl,
        rocket_hook,
    ).await?;

    let globals = globals();

    globals.spawn_task_immediate("rocket launcher", async move {
        async {
            started.rocket.launch().await?;
            TestResult::Ok(())
        }.await.map_err(IE::from)?;
        Ok(TaskWorkComplete {})
    });

    let global::Running { port } = globals.await_running().await.unwrap();

    let tc = TestCtx {
        gl: globals,
        port,
        templates_rendered: Default::default(),
        archive_dir: archive_dir.into_untracked(),
        dummy_repo: dummy_repo.into_untracked(),
        o2m_path: o2m_path.into_untracked(),
    };
    tc.startup(initial_block)?;

    assert_eq!(
        fs::read_to_string(&port_report_file.into_untracked())?,
        format!("{port}\n"),
    );

    Ok(tc)
}

impl TestCtx {

    pub(super) async fn db_await_job<R>(
        &self,
        jid: JobId,
        mut predicate: impl FnMut(Option<&JobRow>) -> Option<R>,
    ) -> Result<R, IE> {
        let mut subscription = self.gl.db_trigger.subscribe();

        loop {
            let job = db_transaction(TN::Readonly, |dbt| {
                let job = dbt.query_row(r#"
                        SELECT * FROM jobs
                         WHERE jid = ?
                    "#,
                    [jid],
                    |row| Ok(JobRow::from_sql_row(row)),
                ).optional().into_internal("query for test")?;

                let job = job.transpose()?;

                Ok::<Option<JobRow>, IE>(job)
            })??;

            if let Some(ret) = predicate(job.as_ref()) {
                return Ok(ret);
            }

            subscription.changed().await.map_err(
                |e| e.into_internal("await trigger in test")
            )?;
        }
    }

    pub(super) fn temp_dir(&self) -> &str {
        self.gl.test_suppl.temp_dir
            // lifetimes ensure this is fine - we borrow self
            .as_path_untracked().to_str().expect("not utf8")
    }
}
