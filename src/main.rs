// Copyright 2024 Ian Jackson and contributors to dgit
// SPDX-License-Identifier: GPL-3.0-or-later
// There is NO WARRANTY.

use tag2upload_service_manager::prelude::*;

fn main() -> Result<(), AE> {
    tag2upload_service_manager::cli_cli::main_entrypoint()
}
