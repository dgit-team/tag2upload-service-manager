
use crate::prelude::*;

use t2umeta_abstract::Value;

/// Values from the tag body and message, showing it's for us
#[derive(Debug, Clone, Deftly)]
#[derive_deftly(FromSqlRow, AsBSqlRow, UpdateSqlRow)]
#[derive_deftly(UiMap, UpdateWorkerReport)]
pub struct Parsed {
    #[deftly(worker_report)]
    pub source: PackageName,
    #[deftly(worker_report)]
    pub version: VersionString,
}

#[derive(Default, Deftly)]
#[derive_deftly(FromTagMessage)]
pub struct Scanned {
    pub please_upload: Option<()>,
    pub distro: HashSet<String>,
    pub source: Option<PackageName>,
    pub version: Option<VersionString>,
}

impl Value for PackageName {}
impl Value for VersionString {}

pub struct PasslistedPackagePattern(pub glob::Pattern);

impl Scanned {
    pub fn parse(self, config: &Config) -> Result<Parsed, NotForUsReason> {
        let Scanned {
            please_upload,
            distro,
            source,
            version,
        } = self;

        let () = please_upload.ok_or_else(|| NFR::NoPleaseUpload)?;
        
        distro.contains(&config.t2u.distro).then_some(())
            .ok_or_else(|| NFR::MetaNotOurDistro)?;

        let source = source.ok_or_else(|| NFR::MissingSource)?;
        let version = version.ok_or_else(|| NFR::MissingVersion)?;

        if let Some(passlist) = &config.testing.allowed_source_packages {
            if ! passlist.iter().any(|pat| pat.matches(&source)) {
                return Err(NFR::PackageNotPasslisted);
            }
        }

        Ok(Parsed {
            source,
            version,
        })
    }
}

impl Parsed {
    pub fn from_tag_message(s: &str) -> Result<Parsed, NotForUsReason> {
        Scanned::from_tag_message(s)?
            .parse(&globals().config)
    }
}
