# Installation etc.

## To install a new version

 1. Make sure you can run `build-repro/attest`
 2. Follow the instructions in `maint/deploy`

## To stop/start the service

As `tag2upload-manager` on `tag2upload-manager-01`,
use the script `service-t2usm`, which is on your PATH.

You can pause processing with `service-t2usm pause KEY PUBLIC-INFO`.
`KEY` is a key that you can use to remove *this* pause, later.
When processing is paused, the whole service is still functional,
but simply won't start new work.  This is good for upgrades.

To stop accepting webhooks, use `service-t2usm throttle` instead.
To resume (whether throttled or paused), `unpause`.

`service-t2usm` can `stop` and `start` like an init script, too.

## To handle a `cargo audit` failure

A full discussion of how to handle advisories in Rust dependencies
is too big to fit into this document.

Most RUSTSEC advisories are harmless in practice.  For example, we
don't care about anything in TLS because we don't *do* TLS (at least,
not in our Rust code - we call git, which does, but that gets updates
via the OS).

Usually, there will be a compatible version of the affected dependency.
Consult RUSTSEC, `lib.rs`/`crates.io`, and the dependency's git forge.
If it seems like simply an upgrade should do it, you can just

```
cargo update -p DEPENDENCY-CRATE-NAME
```

That will edit (only) `Cargo.lock`.  If this solves the problem
and the tests still pass, you can then proceed as for `maint/deploy`.

## To reproduce the release binary

```
rustup toolchain add 1.79.0
apt install libssl-dev pkg-config libsqlite3-dev git build-essential
export CARGO_HOME=/home/rustcargo/.cargo
maint/build-repro rebuild
```

On bookworm amd64.
`/home/rustcargo/.cargo` must be an actual useable `.cargo` directory.

The executable will be checked against the committed
SHA256 (in `.build-repro/artifacts`) and
left in `artifacts/tag2upload-service-manager`.

## To attest a new release binary

As above, but:

`maint/build-repro attest`

You'll want to do this in a (bookworm) chroot.  It generates a commit
containing attestation information including the whole of the
environment.  When running this in a new or modified environment, you
should review the resulting commit to check it doesn't contain any
unwanted personal data.

`build-repro` knows to invoke nailing-cargo if it finds a `Cargo.nail`
in the parent directory.  For nailing-cargo's out-of-tree builds to
work, you will need to ensure that the `SCHROOT_SESSION_ID`
environment variable is preserved when switching to the less
privileged UID.  For example, if using nailing-cargo's support for
using sudo, add `Default env_keep+=SCHROOT_SESSION_ID` to sudo's
configuration inside the chroot.

Alternatively, you can use a long-running schroot session,
made with `schroot -b -n SESSION-NAME -c BASE-CHROOT-NAME`.
You can use runes in `Cargo.nail` to control the chroot, eg:
```
command = [
        "schroot", "-rc", "t2um", "-u", "rustcargo", "--",
        "sh", "-ec", ". $HOME/.profile; exec \"$@\"", "x",
        ]
```

You'll need to install these Debian packages inside the chroot in
addition to those listed above:

```
libtypes-serialiser-perl libdatetime-format-iso8601-perl libdatetime-format-rfc3339-perl sudo
```

## To run the tests

### `cargo test`

```
apt install bash weblint
cargo test --locked
```

### `btest` - tests of built executable

These tests aren't likely to fail so normally you can just rely on CI.

```
apt install bash jq curl netcat-openbsd
btest/run-all
```
