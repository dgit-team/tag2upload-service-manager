
use crate::prelude::*;

struct Fetcher {
    some_forge: &'static dyn SomeForge,
    forge_host: Hostname,
    task_tmpdir: String,
}

pub struct ValidTagObjectData {
    pub tag_data: TagObjectData,
    pub is_recent_enough: IsRecentEnough,
}

pub enum FetchError {
    NotForUs(NotForUsReason),
    Problem(ProcessingError),
}

impl From<NotForUsReason> for FetchError {
    fn from(e: NotForUsReason) -> FetchError {
        FetchError::NotForUs(e)
    }
}

impl<E: Into<ProcessingError>> From<E> for FetchError {
    fn from(e: E) -> FetchError {
        FetchError::Problem(e.into())
    }
}

pub fn record_fetch_outcome(
    mut job: JobInWorkflow,
    tag_data: Result<ValidTagObjectData, FetchError>,
) -> Result<(), IE> {
    let host = &job.data.forge_host;
    let jid = job.jid;

    let (status, info);
    let tag_data: Option<_> = match tag_data {
        Ok(tag_data) => {
            debug!(%host, %jid, "fetched tag OK");
            status = JobStatus::Queued;
            info = format!("tag fetched, ready to process");
            Some(tag_data)
        },
        Err(FetchError::NotForUs(not_for_us)) => {
            debug!(%host, %jid, "found tag but not for us");
            status = JobStatus::NotForUs;
            info = format!("not for us: {}", not_for_us);
            None
        },
        Err(FetchError::Problem(problem)) => {
            info!(%host, %jid, "tag fetch failed: {problem:#}");
            status = JobStatus::Failed;
            info = format!("tag fetch failed: {problem:#}");
            None
        }
    }.into();

    let tag_data: NoneIsEmpty<_> = tag_data.map(|ValidTagObjectData {
        tag_data,
        is_recent_enough: IsRecentEnough { .. },
    }| tag_data).into();

    job.update(
        &db_workflow::Update {
            new_status: Some(status),
            new_info: &info,
        },
        &bsql_update!( JobRow { tag_data } ),
    )?;

    trace!(host=%job.data.forge_host, %jid, "tag fetch work complete");

    Ok(())
}

impl Fetcher {
    async fn make_progress(&self)
        -> Result<(), QuitTask>
    {
        self.some_forge.make_progress(
            &self.forge_host,
            &self.task_tmpdir,
        ).await
    }

    async fn fetch_loop_iteration(
        &self,
    ) -> Result<(), QuitTask> {
        remove_dir_all::ensure_empty_dir(&self.task_tmpdir)
            .with_context(|| self.task_tmpdir.clone())
            .into_internal("create/ensure empty dir")?;

        self.make_progress().await
    }

    pub async fn task(self) -> Result<Void, QuitTask> {
        loop {
            self.fetch_loop_iteration().await?;
        }
    }
}

pub fn start_tasks(globals: &Arc<Globals>) {
    for forge in &globals.config.t2u.forges {
        for sf in {
            forge::FORGES.iter().cloned()
                .filter(|sf| sf.kind_name() == forge.kind)
        } {
            for i in 0..forge.max_concurrent_fetch {
                let task_tmpdir =
                    format!("{}/fetch,{},{},{i}",
                            globals.scratch_dir, sf.namever_str(), forge.host);
                globals.spawn_task_running(
                    format!("fetcher [{} {} {i}]",
                            forge.host, sf.namever_str()),
                    Fetcher {
                        some_forge: sf,
                        forge_host: forge.host.clone(),
                        task_tmpdir,
                    }.task().map(|r| Err(r.void_unwrap_err()))
                );
            }
        }
    }
}
