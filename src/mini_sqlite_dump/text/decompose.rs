
use super::*;

fn char_check_excitement(c: char) -> Result<(), ()> {
    use unicode_properties::{UnicodeGeneralCategory, GeneralCategoryGroup};
    match c {
        '\'' | '\n' => Err(()),
        ' ' | '\t' => Ok(()),
        c => match UnicodeGeneralCategory::general_category_group(c) {
            GeneralCategoryGroup::Separator |
            GeneralCategoryGroup::Other => Err(()),
            GeneralCategoryGroup::Letter |
            GeneralCategoryGroup::Mark |
            GeneralCategoryGroup::Number |
            GeneralCategoryGroup::Punctuation |
            GeneralCategoryGroup::Symbol => Ok(())
        }
    }
}

fn process_text_utf8(
    mut i: &str,
    mut yield_: impl FragmentProcessor,
) -> Result<(), E> {
    loop {
        let mut yield_nice = |s: &str| -> Result<(), E> {
            if ! s.is_empty() {
                yield_(TextFragment::Nice(s))?;
            }
            Ok(())
        };

        let Some((nice, exciting)) = i
            .find(|c| char_check_excitement(c).is_err())
            .map(|exciting| i.split_at(exciting))
        else {
            return yield_nice(i);
        };

        yield_nice(nice)?;

        let mut chars = exciting.chars();
        let ch = chars.next().expect("found, earlier");
        let rhs = chars.as_str();
        let s = &exciting[0.. exciting.len() - rhs.len()];
        i = rhs;

        if ch == '\'' {
            yield_(TextFragment::Squote(s))?;
        } else {
            yield_(TextFragment::Char(ch, s))?;
        }
    }
}

pub fn process_text_bytes(
    i: &[u8],
    mut yield_: impl FragmentProcessor,
) -> Result<(), E> {
    for ch in i.utf8_chunks() {
        process_text_utf8(ch.valid(), &mut yield_)?;
        let n = ch.invalid();
        if ! n.is_empty() {
            yield_(TextFragment::Bad(n))?;
        }
    }
    Ok(())
}
