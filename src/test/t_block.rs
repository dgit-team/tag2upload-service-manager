//! Blocking the mainline code, and unblocking iot
//!
//! Helps the test cases control event sequencing

use super::*;

#[derive(Debug)]
pub(super) struct BlockBlocks(
    /// Current blocks
    ///
    /// `Vec` so that the one we find is deterministic.
    Vec<BlockEntry>
);

#[derive(Debug)]
struct BlockEntry {
    pat: glob::Pattern,

    /// Hook points that are blocked on this
    ///
    /// Use of a `Mutex` is a little unprincipled here,
    /// but that saves us having to tell everyone we're updating the
    /// data structure when we add a point here.
    ///
    /// We don't do any kind of Drop handling to remove entries.
    /// The `blocked` is discarded with the whole `BlockEntry`
    /// in `unblock`.
    ///
    /// Consequences include:
    ///  * Trying to wait for something to be blocked here won't work
    ///    since we don't trip the watch.
    ///  * If a task is cancelled while blocked on a hook point,
    ///    it may still show up here.
    blocked: Mutex<HashSet<String>>,
}

#[derive(Debug)]
pub(super) struct BlockHook(glob::Pattern);

struct IsBlocked;

impl BlockEntry {
    fn new(pat: glob::Pattern) -> Self {
        BlockEntry { pat, blocked: Default::default() }
    }
}

impl BlockBlocks {
    fn insert_block(&mut self, pat: glob::Pattern) {
        // If there's a need for duplicate blocks, we must decide
        // on what basis to prefer one entry over another.
        self.0.iter().for_each(|e| assert_ne!(e.pat, pat));
        let ent = BlockEntry::new(pat);
        self.0.push(ent);
    }
    fn remove_block(&mut self, pat: &glob::Pattern) {
        let mut removed = 0;
        self.0.retain(|e| {
            let remove = &e.pat == pat;
            if remove { removed += 1 }
            !remove
        });
        assert_eq!(removed, 1, "{pat:?}");
    }
    fn is_blocked(&self, point: &str) -> Option<IsBlocked> {
        self.0.iter()
            .find(|ent| ent.pat.matches(&point))
            .map(|ent| {
                ent.blocked.lock().expect("poisoned")
                    .insert(point.to_owned());
                IsBlocked
            })
    }
}

impl Globals {
    pub(super) fn t_block(&self, pattern: &str) -> BlockHook {
        let pat: glob::Pattern = pattern.parse().expect("bad pattern");
        self.state.send_modify(|ss| {
            ss.test_suppl.blocks
                .insert_block(pat.clone());
        });
        BlockHook(pat)
    }

    pub(super) async fn t_hook_blocks(&self, point: &str) {
        let _: Result<watch::Ref<_>, watch::error::RecvError> =
            self.state
                .subscribe()
                .wait_for(|ss| {
                    ss.test_suppl.blocks.is_blocked(point).is_none()
                })
                .await;
    }

    pub(super) fn points_blocked_by(&self, pat: &BlockHook)
                                    -> HashSet<String>
    {
        self.state.borrow()
            .test_suppl.blocks.0
            .iter()
            .filter(|e| e.pat == pat.0)
            .exactly_one().expect("BlockHook not unique?!")
            .blocked
            .lock().expect("poisoned")
            .clone()
    }
}

impl TestCtx {
    #[allow(dead_code)]
    pub(super) fn block(&self, pattern: &str) -> BlockHook {
        self.gl.t_block(pattern)
    }

    pub(super) fn unblock(&self, pat: BlockHook) {
        self.gl.state.send_modify(|ss| {
            ss.test_suppl.blocks
                .remove_block(&pat.0);
        });
    }
}

impl BlockBlocks {
    pub(super) fn new_all() -> (Self, BlockHook) {
        let pat: glob::Pattern = "*".parse().unwrap();
        let ent = BlockEntry::new(pat.clone());
        (
            BlockBlocks([ent].into_iter().collect()),
            BlockHook(pat),
        )
    }
}
