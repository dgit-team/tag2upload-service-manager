//! Test case data

use super::*;

/// Payload of an actual live gitlab webhook reqeust.
///
/// From the upload of dgit-test-dummy 1.40.
///
/// C&P from the gitlab web UI:
/// <https://salsa.debian.org/dgit-team/dgit-test-dummy/-/hooks/211306/edit>
///
/// The web ui presents a nicely formatted version of the payload;
/// I think the real payload doesn't have all that whitespace.
/// My test script reported `CONTENT_LENGTH=2765`;
/// this file is 3054 bytes, but feeding it through `jq -c` gives 2686.
/// The remaining discrepancy of 79 might be the redacted token.
pub const HOOK_BODY: &str = include_str!("hook_body_gitlab.json");

pub const HOOK_BODY_UI_TEST: &str = include_str!("gitlab-test_hook_body.json");

/// Tag name for the above version
pub const TAG_NAME: &str = "debian/1.40";

pub const FORGE_HOST: &str = "salsa.debian.org";

/// The [`mock_gitlab_tag_info`] route returns this file.
///
/// Result of
/// ```text
/// curl https://salsa.debian.org/api/v4/projects/36575/repository/tags/debian%2f1.40 >src/test/tag_api_resp.json
/// ```
/// See in [`gitlab::make_progress`]
pub const GL_API_RESP: &str = include_str!("tag_api_resp.json");

/// Tarball for simulating the git clone
///
/// Result of
/// ```text
/// cd src/test
/// tar atf dgit-test-dummy.git.tar.xz 
/// git clone --bare ~/things/Dgit/dgit-test-dummy/
/// cd dgit-test-dummy.git
/// git prune
/// git gc --aggressive
/// cd ..
/// tar Jcf dgit-test-dummy.git.tar.xz dgit-test-dummy.git
/// ```
pub const DUMMY_REPO_TARBALL: &str =
    "src/test/dgit-test-dummy.git.tar.xz";

/// Time when we pretend the test case runs
///
/// Must be after the test case tag, but not by so much that it's too old.
pub const SIMULATED_START_TIME: &str = "2024-09-07T12:52:58Z";

/// Project ID in the test case webhook [`HOOK_BODY`]
pub const GL_PROJECT: gitlab::ProjectId = gitlab::ProjectId(36575);

pub const VHOST_UI: &str = "tag2upload.debian.org";
pub const VHOST_WEBHOOK: &str = "webhook.tag2upload.debian.org";

#[rocket::get("/api/v4/projects/<project_id>/repository/tags/<tag_name>")]
pub async fn r_mock_gitlab_tag_info(
    project_id: u64,
    tag_name: &str,
) -> Result<String, rocket::http::Status> {
    info!("mocking {project_id:?} {tag_name:?}");
    Ok(match (gitlab::ProjectId(project_id), &*tag_name) {
        (GL_PROJECT, TAG_NAME) => GL_API_RESP,
        _ => return Err(rocket::http::Status::NotFound)
    }.to_string())
}

pub fn internal_error_hook(ie: &InternalError) {
    eprintln!(
        "{}\n** internal error! aborting! **\n",
        ie.display_with_backtrace(),
    );
    globals().state.borrow().test_suppl.abort.abort()
}
