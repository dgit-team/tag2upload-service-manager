# Copyright 2024 Ian Jackson and contributors to dgit
# SPDX-License-Identifier: GPL-3.0-or-later
# There is NO WARRANTY.

stages:
  - build
  - test
  - comprehensive
  - build-repro

# When changing this, `git grep bookworm`.
image: "rust:1.79.0-bookworm"

variables:
  # https://dev.to/zenika/gitlab-ci-optimization-15-tips-for-faster-pipelines-55al
  # https://docs.gitlab.com/runner/configuration/feature-flags.html
  FF_USE_FASTZIP: "true"
  # For integration tests with dgit.git
  DGIT_REPO: https://salsa.debian.org/dgit-team/dgit.git
  DGIT_MAIN: master
  DEBIAN_FRONTEND: noninteractive

.rust:
  cache:
    paths:
      - target
  before_script:
    - mkdir -p target
    - find target -mtime +1.1 -type f -print0 | xargs -0r rm --

cargo-clippy:
  stage: test
  extends: .rust
  script:
    - rustup component add clippy
    # we must run this twice, so we run clippy without cfg(test)
    - cargo clippy --locked --workspace --all-features
    - cargo clippy --locked --workspace --all-features --all-targets

.shared-definitions:
  - &cargo-test-prep-script
    - maint/common/apt-install weblint-perl sqlite3
  - &btest-prep-script
    - maint/common/apt-install netcat-openbsd jq sqlite3
  - &every-commit-prep-script
    - maint/common/apt-install libtoml-perl git
  - &build-t2u-binary-script
    export DGIT_TEST_T2USM_PROGRAM=$(pwd)/artifacts/tag2upload-service-manager

cargo-test:
  stage: test
  extends: .rust
  script:
    - *cargo-test-prep-script
    - cargo test --locked --workspace --all-features

# This can start to fail even when our code doesn't change.
# Usually the new advisory is not a huge concern.
cargo-audit:
  # Run it last, separately, so if we think we may want to merge anyway,
  stage: build-repro
  rules:
    # Don't impede MR work.
    # Also, don't run it for tags, so we don't disrupt releases.
    - if: $CI_COMMIT_BRANCH == "main"
  script:
    - maint/common/via-cargo-install-in-ci cargo-audit --version 0.20.1
    - maint/cargo-audit
  cache:
    key: via-cargo-install-in-ci
    paths:
      - cache

every-commit-cargo:
  stage: comprehensive
  extends: .rust
  variables:
    T2USM_COVERAGE_TESTS: 0
  script:
    - *cargo-test-prep-script
    - *every-commit-prep-script
    - maint/common/for-every-commit cargo test --locked --workspace --all-features

every-commit-btest:
  stage: comprehensive
  extends: .rust
  script:
    - *btest-prep-script
    - *every-commit-prep-script
    - maint/common/for-every-commit btest/build-run-all

check-licences:
  stage: comprehensive
  script:
    - maint/common/via-cargo-install-in-ci cargo-license --version 0.6.1
    - maint/check-licences
  cache:
    key: via-cargo-install-in-ci
    paths:
      - cache

build:
  # The results from this job will be used for integration
  # tests with the dgit test suite
  #
  # Also, running this early means we make a cache of our built dependencies,
  # which speeds up the clippy and test jobs etc.
  stage: build
  extends: .rust
  script:
    - cargo build --locked --workspace --all-features
    - rm -rf artifacts
    - mkdir artifacts
    - mv target/debug/tag2upload-service-manager artifacts/.
  artifacts:
    paths:
      - artifacts

btest:
  stage: test
  script:
    - *build-t2u-binary-script
    - *btest-prep-script
    - btest/run-all

cargo-test-release:
  stage: comprehensive
  extends: .rust
  script:
    - *cargo-test-prep-script
    - cargo test --locked --workspace --all-features --release
  cache:
    # Mostly, things built with and without --release aren't shareable
    key: profile-release

t2u-integration:
  stage: test
  # Empirically, rust:1.79.0-bookworm fails with some crazy behaviour
  # from pbuilder's postinst.
  image: debian:bookworm
  script:
    - maint/common/apt-install git
    - git clone --depth 1 --no-tags $DGIT_REPO -b $DGIT_MAIN dgit
    - *build-t2u-binary-script
    - cd dgit
    # tests/gitlab-ci-run-all wants .debs in debian/output/, because the
    # Debian Salsa CI pipeliens put build artifacts there, and it uses these
    # to satisfy the dependencies.  In theory we could reuse artifacts,
    # but then we'd need to resue the artifact source code too (or risk
    # mismatches) and the artifact source is a .dsc, not a git tree.
    - apt-get -y build-dep .
    - dpkg-buildpackage -uc -b
    - mkdir debian/output
    - mv -v ../*.deb debian/output/
    - export TESTSCRIPTS=$(tests/list-t2u-integration-tests)
    - tests/gitlab-ci-run-all
  after_script:
    - cd dgit
    - tests/maybe-tail-log-of-one-failing-test
  artifacts:
    name: logs
    when: always
    expire_in: 1 week
    paths:
      - dgit/tests/tmp/*

check-misc:
  stage: test
  script:
    - maint/common/apt-install shellcheck
    - maint/common/forbid-hard-tabs ':*.rs'
    - maint/common/shellcheck-all

# This should always be in the last testing stage, so that if it fails all the other steps still run
# But it should run before any deployument.
blocking-todos:
  stage: comprehensive
  needs: []
  script:
    - maint/check-blocking-todos

# What happens if we use the latest upstream stable Rust
# and the latest allegedly-semver-compatible versions of our dependencies?
latest-upstreams:
  extends: .rust
  stage: comprehensive
  allow_failure: true
  image: rust:bookworm
  script:
    - *cargo-test-prep-script
    - rustc -vV
    - cargo update
    - cargo build --workspace --all-features
    - cargo test --workspace --all-features
  cache:
    key: latest-upstreams

build-repro:
  stage: build-repro
  extends: .rust
  variables:
    CARGO_HOME: /home/rustcargo/.cargo
  script:
    - rm -rf artifacts
    - rustc -V
    - mkdir -p $CARGO_HOME
    - maint/build-repro ci-rebuild
  artifacts:
    paths:
      - artifacts
  cache:
    # We set a CARGO_HOME which ends up captured, so have a separate cache
    key: build-repro
# Notes:
# I can (currently) repro the generated executable with this rune
#   nailing-cargo build --release --all-features
# in a bookworm chroot with upstream Rust 1.79.0 from rustup
