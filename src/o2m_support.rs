
use crate::prelude::*;

use tokio_util::codec::{FramedRead, LinesCodec};

pub use BadMessage as BM;

#[derive(Debug, Clone, Copy)]
pub enum ExpectedKeyword {
    NeedsRecase(&'static str),
    Literal(&'static str),
}

impl Display for ExpectedKeyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use ExpectedKeyword as EK;
        match self {
            EK::NeedsRecase(s) => Display::fmt(&AsKebabCase(s), f),
            EK::Literal(s) => Display::fmt(s, f),
        }
    }
}

//---------- connection wrapper ----------

pub struct OracleConnection {
    rx: FramedRead<Pin<Box<dyn AsyncRead + Send + Sync>>, LinesCodec>,
    tx: Pin<Box<dyn AsyncWrite + Send + Sync>>,
    id: OracleConnectionId,
    tracker: TrackedWorker,
}

pub type OracleConnectionId = Either<u64, types::WorkerId>;

//---------- formatting messages for transmission ----------

pub trait MessageToOracle {
    /// Includes the trailing newline
    fn write(&self, out: &mut String);
}

//---------- parsing received messages ----------

pub trait MessageFromOracle: Sized {
    type Error;

    /// `Ok(None)` if the keyword didn't match
    fn maybe_parse(
        m: &mut IncomingMessageRhs,
    ) -> Result<Option<Self>, Self::Error>;
}

#[derive(Clone)]
pub struct IncomingMessageRhs<'s> {
    keyword: &'s str,

    /// Never contains leading whitespace
    s: &'s str,

    expected_keywords: Vec<ExpectedKeyword>,
}

#[derive(Error, Debug, Clone)]
pub enum BadMessage {
    #[error("keyword missing - empty line!")]
    EmptyMessage,

    #[error("in message {keyword}, data for {field} has bad syntax")]
    FieldSyntax { keyword: ExpectedKeyword, field: &'static str },

    #[error("in message {keyword}, data for {field} missing")]
    MissingField { keyword: ExpectedKeyword, field: &'static str },

    #[error("in message {keyword}, extra data after end of expected fields")]
    ExtraFields { keyword: String },

    #[error(
        "message received out of sequence; got {got:?}, expected {}",
        {
            let mut s = String::new();
            for (slash, e) in izip!(
                iter::once("").chain(iter::repeat("/")),
                expected,
            ) {
                write!(s, "{slash}{e}").expect("write to String failed");
            }
            s
        }
    )]
    UnexpectedMessage {
        got: String,
        expected: Vec<ExpectedKeyword>,
    },

    #[error(
        "message received out of sequence; got {got:?} when nothing expected",
    )]
    MessageSequencingError {
        got: String,
    },
}

//---------- implementation, connection wrapper ----------

impl OracleConnection {
    pub fn from_unix_socket(socket: UnixStream, seq: u64) -> Result<Self, IE> {
        let gl = globals();
        let (rx, tx) = socket.into_split();
        let codec = LinesCodec::new_with_max_length(
            gl.config.limits.o2m_line
        );
        let rx = FramedRead::new(Box::pin(rx) as _, codec);
        let tx = Box::pin(tx);
        let id = Either::Left(seq);
        let report = WorkerReport {
            last_contact: gl.now(),
            ident: id.to_string(),
            phase: WorkerPhase::Init,
            source: None.into(),
            version: None.into(),
            status: None,
            info: None,
        };
        let tracker = gl.worker_tracker.clone().new_worker(report);
        Ok(OracleConnection { tx, rx, id, tracker })
    }

    /// Receive a message matching T
    ///
    /// # Cancellation safety
    ///
    /// This method is cancel safe.
    /// If the returned future is dropped before it completes,
    /// no input will be lost.
    pub async fn recv<T: MessageFromOracle>(
        &mut self,
    ) -> Result<T, OracleTaskError>
    where OracleTaskError: From<T::Error>,
    {
        use tokio_util::codec::LinesCodecError as LCE;

        let l: String = self.rx.next().await.ok_or(OTE::Disconnected)?
            .map_err(|e| match e {
                LCE::MaxLineLengthExceeded => OTE::MaxLineLengthExceeded,
                LCE::Io(ioe) => OTE::Io(ioe.into()),
            })?;

        trace!("{self} < {}", l.escape_default());

        let mut temp_im = IncomingMessageRhs {
            keyword: "", // for the moment
            s: &l,
            expected_keywords: vec![],
        };

        let keyword = temp_im.next().ok_or(BM::EmptyMessage)?;

        let mut im = IncomingMessageRhs { keyword, ..temp_im };

        let t = MessageFromOracle::maybe_parse(&mut im)?
            .ok_or_else(|| {
                let expected = mem::take(&mut im.expected_keywords);

                match MessageFromOracle::maybe_parse(&mut im).void_unwrap() {
                    Some(violation) =>
                        OTE::PeerReportedProtocolViolation(violation),
                    None => BM::UnexpectedMessage {
                        got: im.keyword.to_owned(),
                        expected,
                    }.into(),
                }
            })?;

        im.s.trim_end().is_empty().then_some(())
            .ok_or_else(|| BadMessage::ExtraFields {
                keyword: im.keyword.to_owned(),
            })?;

        Ok(t)
    }

    pub async fn xmit<T: MessageToOracle>(
        &mut self,
        t: &T,
    ) -> Result<(), OracleTaskError> {
        let mut s = String::new();
        t.write(&mut s);

        let (l, data) = s.split_once('\n').ok_or_else(
            || internal!("MessageToOracle impl output has no newline")
        )?;

        trace!("{self} > {}", l.escape_default());
        if data != "" {
            trace!("{self} >...");
        }

        async {
            self.tx.write_all(s.as_ref()).await?;
            self.tx.flush().await
        }.await
            .map_err(|e| if e.kind() == io::ErrorKind::BrokenPipe {
                OTE::Disconnected
            } else {
                OTE::Io(e.into())
            })?;

        Ok(())
    }

    pub fn update_report(
        &mut self,
        phase: WorkerPhase,
        job_row: Option<&JobRow>,
        f: impl FnOnce(&mut WorkerReport),
    ) -> Result<(), IE> {
        let now = globals().now();
        self.tracker.update(|wt| {
            wt.last_contact = now;
            wt.phase = phase;
            if let Some(job_row) = job_row {
                job_row.data.tag_meta.update_worker_report(wt);
                job_row.update_worker_report(wt);
            }
            f(wt)
        });
        Ok(())
    }

    pub fn set_worker_id(&mut self, id: types::WorkerId) -> Result<(), IE> {
        self.id = Either::Right(id);
        let ident = self.id.to_string();
        self.update_report(WorkerPhase::Init, None, |wt| {
            wt.ident = ident;
        })
    }
}

impl Display for OracleConnection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.id {
            Either::Left(seq) => write!(f, "#{seq}"),
            Either::Right(worker) => write!(f, "{worker}"),
        }
    }
}

impl Debug for OracleConnection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("OracleConnection")
            .field("id", &self.id)
            .finish()
    }
}

//---------- implementation, formatting ----------

pub fn write_data_blocks(data: &dyn Display, out: &mut String) -> fmt::Result {
    let data = data.to_string();
    writeln!(out, "data-block {}\n{data}data-end", data.len())
}

define_derive_deftly! {
    MessageToOracle expect items:

    // NB this is duplicated with MessdageToOracle, above
    ${define KEYWORD ${if tmeta(o2m(keyword)) {
        ExpectedKeyword::Literal(${tmeta(o2m(keyword)) as str})
    } else {
        ExpectedKeyword::NeedsRecase(stringify!($tname))
    }}}

    ${defcond F_REST_OF_LINE fmeta(o2m(rest_of_line))}

    ${defcond F_DATA_BLOCKS fmeta(o2m(data_blocks))}

    impl $crate::o2m_support::MessageToOracle for $ttype {
        fn write(&self, out: &mut String) {
            #[allow(unused_imports)] // TODO false positive
            use $crate::o2m_support::*;

            ${if is_enum {
                match self { $(
                    $vtype(v) => MessageToOracle::write(v, out);
                ) }
            } else {
                (|| {
                    write!(out, "{}", $KEYWORD)?;
                    $(
                        ${when not(F_DATA_BLOCKS)}
                        write!(out, " {}", &self.$fname)?;
                    )
                    writeln!(out)?;
                    $(
                        ${when F_DATA_BLOCKS}
                        write_data_blocks(&self.$fname, out)?;
                    )
                    Ok::<_, fmt::Error>(())
                })().expect("write to string");
            }}
        }
    }
}

//---------- implementation, parsing ----------

impl<'s> Iterator for IncomingMessageRhs<'s> {
    type Item = &'s str;
    fn next(&mut self) -> Option<&'s str> {
        if let Some((l, r)) = self.s
            .split_once(|c: char| c.is_ascii_whitespace())
        {
            self.s = r.trim_start();
            Some(l)
        } else {
            let r = self.rest_of_line();
            if r.is_empty() {
                None
            } else {
                Some(r)
            }
        }
    }
}

impl<'s> IncomingMessageRhs<'s> {
    pub fn is_keyword(&mut self, keyword: ExpectedKeyword) -> Option<()> {
        self.expected_keywords.push(keyword);

        display_eq(self.keyword, keyword).then_some(())
    }

    pub fn rest_of_line(&mut self) -> &'s str {
        mem::replace(&mut self.s, "")
    }
}

/// Expects no message to be available; if one appears, throws
impl MessageFromOracle for Void {
    type Error = BadMessage;
    fn maybe_parse(
        m: &mut IncomingMessageRhs,
    ) -> Result<Option<Self>, Self::Error> {
        Err(BadMessage::MessageSequencingError {
            got: m.keyword.to_owned(),
        })
    }
}

define_derive_deftly! {
    MessageFromOracle expect items:

    // NB this is duplicated with MessdageToOracle, above
    ${define KEYWORD ${if tmeta(o2m(keyword)) {
        ExpectedKeyword::Literal(${tmeta(o2m(keyword)) as str})
    } else {
        ExpectedKeyword::NeedsRecase(stringify!($tname))
    }}}

    ${defcond F_REST_OF_LINE fmeta(o2m(rest_of_line))}

    impl $crate::o2m_support::MessageFromOracle for $ttype {
        type Error = ${if tmeta(o2m(infallible)) {
            void::Void
        } else {
            $crate::o2m_support::BadMessage
        }};

        fn maybe_parse(
            m: &mut $crate::o2m_support::IncomingMessageRhs,
        ) -> Result<Option<Self>, Self::Error> {
            #[allow(unused_imports)] // TODO false positive
            use $crate::o2m_support::*;

            ${if is_enum {
                $( {
                    let mut m_try = m.clone();
                    if let Some(v) = MessageFromOracle::maybe_parse(
                        &mut m_try
                    )? {
                        *m = m_try;
                        return Ok(Some($vtype(v)));
                    }
                } )
                Ok(None)
            } else {
                let Some(()) = m.is_keyword($KEYWORD)
                else { return Ok(None) };

                Ok(Some($tname { $(
                    $fname: ${if F_REST_OF_LINE {
                            m.rest_of_line()
                        } else {
                            m.next().ok_or_else(|| BM::MissingField {
                                keyword: $KEYWORD,
                                field: stringify!($fname),
                            })?
                        }}
                        .parse()
                      ${if tmeta(o2m(infallible)) {
                        .map_err(|e| match e {})
                      } else {
                        .map_err(|_| BM::FieldSyntax {
                            keyword: $KEYWORD,
                            field: stringify!($fname),
                        })
                      }}
                        ?,
                ) }))
            }}
        }
    }
}
