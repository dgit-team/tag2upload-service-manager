pub use std::{
    any::Any,
    borrow::Cow,
    cell::RefCell,
    cmp,
    collections::{BTreeMap, HashMap, HashSet},
    ffi::{c_int, CString},
    fmt::{self, Debug, Display, Write as _},
    fs,
    future::Future,
    hash::Hash,
    io::{self, Cursor, Write as _},
    iter,
    marker::PhantomData,
    mem,
    net::IpAddr,
    num::NonZeroU8,
    ops::Deref,
    panic::AssertUnwindSafe,
    pin::Pin,
    rc::Rc,
    str::{self, FromStr},
    sync::{Arc, OnceLock},
    time::{Duration, SystemTime},
};

pub use rocket::{async_trait, figment, futures, serde::json::serde_json};
pub use humantime_serde::re::humantime;

pub use futures::{future, FutureExt as _, SinkExt as _};

pub use {
    anyhow::{anyhow, Context as _},
    derive_deftly::define_derive_deftly,
    derive_deftly::{derive_deftly_adhoc, Deftly},
    derive_more::derive::{Deref, DerefMut, From, Into},
    easy_ext::ext,
    figment::{Figment, providers::Format as _},
    heck::AsKebabCase,
    ipnet::IpNet,
    itertools::{Itertools, Either, chain, izip},
    maybe_owned::MaybeOwned,
    paste::paste,
    reqwest::Url,
    rocket::fairing,
    rusqlite::{ToSql, OptionalExtension as _},
    rusqlite::types::{ToSqlOutput, FromSql, FromSqlResult},
    serde::{Deserialize, Deserializer, Serialize, Serializer},
    serde_json::json,
    serde::{de::Error as _},
    slab::Slab,
    strum::{IntoEnumIterator as _},
    tera::Tera,
    thiserror::Error,
    tokio::{select, sync::watch},
    tokio::io::{AsyncRead, AsyncReadExt as _},
    tokio::io::{AsyncWrite, AsyncWriteExt as _},
    tokio::net::{UnixStream},
    tokio::sync::oneshot,
    tokio_stream::{StreamExt as _},
    tracing::{trace, debug, info, warn, error},
    void::{Void, ResultVoidExt as _, ResultVoidErrExt as _},
};

pub use crate::{
    bsql_queries, bsql_rows, cli_common, config, db_data,
    db_support, db_workflow, dns, error, expire, fetcher,
    fmt_cmp, forge, gitclone, gitlab, global, logging, mini_sqlite_dump,
    o2m_listener, o2m_messages, o2m_support, o2m_tracker,
    t2umeta, t2umeta_abstract, tracing_logrotate, types,
    types_string_abstract, ui_abstract, ui_render, ui_routes, ui_vhost,
    utils, webhook,
};
pub use crate::{
    CodeLocation, CodeLocationAccumulator,
};

pub(crate) use crate::{test};

pub use {
    config::Config,
    cli_common::{CliOperation, CliOptions, WholeConfig},
    bsql_rows::{FromSqlRow, UpdateSqlRow as _},
    bsql_queries::{BoundSql, RusqliteConnectionExt as _},
    bsql_queries::{RusqliteTransactionExt as _},
    db_data::{JobData, JobDataUpdate},
    db_data::{JobRow, JobRowUpdate, JobStatus, PauseInsn},
    db_support::{db_transaction},
    db_support::{TransactionNature},
    db_workflow::{JobInWorkflow, check_not_throttled},
    db_workflow::{db_jobs_for_ui, db_query_for_ui, query_pause_insn_either},
    dns::{AllowedCaller, DnsGlobPattern, IsAllowedCaller},
    error::{InternalError, IntoInternal as _},
    error::{IntoInternalOption as _, IntoInternalResult as _},
    error::{MismatchError, NotForUsReason, QuitTask, OracleTaskError},
    error::{ProcessingError, StartupError},
    error::{TaskResult, TaskWorkComplete, WebError},
    fetcher::{record_fetch_outcome, FetchError, ValidTagObjectData},
    fmt_cmp::display_eq,
    forge::SomeForge,
    global::{globals, test_hook},
    global::{DbAssocState, Globals, IsPaused, IsThrottled, ShuttingDown},
    o2m_tracker::{TrackedWorker, WorkerPhase, WorkerReport, WorkerTracker},
    o2m_tracker::derive_deftly_template_UpdateWorkerReport,
    t2umeta_abstract::{FromTagMessage, derive_deftly_template_FromTagMessage},
    t2umeta_abstract::{MetadataItemError},
    types::{ForgeData, ForgeNamever, Hostname, JobId},
    types::{UnexpectedNullGitObjectId, GitObjectId, GitObjectIdOrNull},
    types::{PackageName, PauseId, ProcessingInfo},
    types::{TagObjectData, TimeT, VersionString},
    types_string_abstract::{NoneIsEmpty, SuitableForNoneIsEmpty},
    ui_abstract::{UiDisplay, UiMap, UiSerializeRows},
    ui_render::UiReqInfo,
    utils::{HtDuration, HtTimeT, IsRecentEnough},
    utils::{unix_access, WatchReceiverExt},
    webhook::{RawSpecificWebhookPayload},
};

pub type RocketBuild = rocket::Rocket<rocket::Build>;
pub type RocketIgnite = rocket::Rocket<rocket::Ignite>;

pub use anyhow::Error as AE;
pub use InternalError as IE;
pub use NotForUsReason as NFR;
pub use OracleTaskError as OTE;
pub use ProcessingError as PE;
pub use TransactionNature as TN;
pub use WebError as WE;

#[cfg(test)]
pub mod test_prelude {
    pub use super::*;
    pub use lazy_regex::regex;
    pub use serde_json::json;
    pub use std::collections::{BTreeSet, HashSet};
    pub use std::sync::{Mutex, MutexGuard};
    pub use testresult::{TestError, TestResult};
    pub use test_temp_dir::{test_temp_dir, TestTempDir, TestTempDirGuard};
    pub use tokio::task::AbortHandle;
    pub use tokio_util::codec::{Framed, LinesCodec};
    pub use tracing_test::traced_test;

    pub use bsql_queries::{IsFragment as _};
}
