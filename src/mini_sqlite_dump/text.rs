
use super::*;

mod decompose;
mod reassemble;

#[cfg(test)]
mod test;

#[derive(Debug)]
enum TextFragment<'s> {
    Nice(&'s str),
    Squote(&'s str),
    Char(char, &'s str),
    Bad(&'s [u8]),
}

trait FragmentProcessor: FnMut(TextFragment) -> Result<(), E> {}
impl<F> FragmentProcessor for F
where F: FnMut(TextFragment) -> Result<(), E> {}

fn write_text_debug(
    w: impl io::Write,
    text: &[u8],
    debug: &mut impl DebugWrite,
) -> Result<(), E> {
    let mut frag_writer = reassemble::FragmentWriter::new(w);
    decompose::process_text_bytes(
        text,
        |frag| {
            if debug.enabled() {
                write!(debug, "{frag:?}")?;
            }
            frag_writer.write_fragment(frag)
        },
    )?;
    frag_writer.finish()
}

pub fn write_text(w: impl io::Write, text: &[u8]) -> Result<(), E> {
    write_text_debug(w, text, &mut io::sink())
}

#[ext(FmtResultExt)]
impl fmt::Result {
    fn expect_write(self) {
        self.expect("write failed");
    }
}

//---------- debugging ----------

trait DebugWrite: io::Write {
    fn enabled(&self) -> bool;
}

impl<W: io::Write> DebugWrite for io::LineWriter<W> {
    #[inline(always)]
    fn enabled(&self) -> bool { true }
}
impl DebugWrite for io::Sink {
    #[inline(always)]
    fn enabled(&self) -> bool { false }
}
