
use crate::prelude::*;

define_string_newtypes! {
    /// Cannot be 0000 for "none"
    GitObjectId;
    
    #[derive(From)]
    ForgeNamever;
}

//---------- TimeT ----------

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Deftly, From, Into)]
#[derive_deftly(ToFromSqlInner)]
pub struct TimeT(u64);

impl Display for TimeT {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let st = match SystemTime::try_from(*self) {
            Ok(y) => y,
            Err(e) => return write!(
                f, "<error printing time {}! {e}>", self.0
            ),
        };
        Display::fmt(&humantime::format_rfc3339(st), f)
    }
}

impl Debug for TimeT {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "TimeT({} {self})", self.0)
    }
}

impl From<SystemTime> for TimeT {
    fn from(st: SystemTime) -> TimeT {
        let time_t = st.duration_since(SystemTime::UNIX_EPOCH)
            .unwrap_or(Duration::ZERO)
            .as_secs();
        TimeT(time_t)
    }
}

fn system_time_max() -> SystemTime {
    SystemTime::UNIX_EPOCH
        .checked_add(Duration::from_secs(i64::MAX as u64))
        .expect("time_t (i64::MAX) must fit into SystemTime!")
}

#[test]
fn check_system_time_max() { system_time_max(); }

impl From<TimeT> for SystemTime {
    fn from(time_t: TimeT) -> SystemTime {
        SystemTime::UNIX_EPOCH
            .checked_add(Duration::from_secs(time_t.0))
            .unwrap_or_else(system_time_max)
    }
}

impl TimeT {
    pub fn checked_sub(self, delta: u64) -> Option<TimeT> {
        self.0.checked_sub(delta).map(TimeT)
    }
}

ui_display_via_to_string! { TimeT }

//---------- JobId, PauseId ----------

macro_rules! define_sqlite_rowid { { $name:ident } => {
    #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
    #[derive(Deftly, derive_more::Display, derive_more::FromStr)]
    #[derive_deftly(ToFromSqlInner)]
    #[display("{}", self.0)]
    pub struct $name(db_support::RowId);

    impl $name {
        pub fn none() -> Self {
            $name(0)
        }
    }

    ui_display_via_to_string! { $name }
} }

define_sqlite_rowid!(JobId);
define_sqlite_rowid!(PauseId);

//---------- Hostname ----------

define_standard_string_newtype! {
    Hostname [
        error("syntactically invalid hostname");
    ]:

    fn char_ok(c: char) -> bool {
        c.is_ascii_alphanumeric() || c == '-' || c == '.'
    }

    fn other_checks(s: &str) -> Result<(), AE> {
        let comps = s.split('.');
        for comp in comps {
            if comp.is_empty() {
                return Err(anyhow!("empty label (or initial/final dot)"));
            }
            if !comp.starts_with(|c: char| c.is_ascii_alphanumeric()) {
                return Err(anyhow!("label must start with alphanumeric"));
            }
            if comp.len() > 253 {
                return Err(anyhow!("label too long (>263 chars)"));
            }
        }
        Ok(())
    }
}

#[test]
fn hostname_tests() {
    let ok = |s: &str| { s.parse::<Hostname>().expect(s); };
    let err = |s: &str| { s.parse::<Hostname>().expect_err(s); };

    ok("1");
    ok("1-");
    ok("a");
    ok("a.b");
    ok("a.b.c");
    err("");
    err("-a");
    err("a.");
    err(".a");
    ok("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789abc");
    err("0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789abcd");
}

//---------- GitObjectId ----------

#[derive(Error, Debug)]
#[error("syntactically invalid git object id: {0:#}")]
pub struct InvalidGitObjectId(AE);

define_standard_string_newtype! {
    /// Might be 0000 for "none"
    GitObjectIdOrNull []:

    type Error = InvalidGitObjectId;

    fn char_ok(c: char) -> bool {
        c.is_ascii_hexdigit()
    }
    fn normalise(s: String) -> Result<String, AE> {
        Ok(s.to_ascii_lowercase())
    }
    fn map_err(ae: AE) -> Self::Error { InvalidGitObjectId(ae) }
}

#[derive(Error, Debug)]
#[error("null git object id found where non-null id expected")]
pub struct UnexpectedNullGitObjectId;

#[derive(Error, Debug)]
pub enum InvalidOrNullGitObjectId {
    #[error("{0}")]
    Null(#[from] UnexpectedNullGitObjectId),

    #[error("{0}")]
    Invalid(#[from] InvalidGitObjectId),
}

impl TryFrom<GitObjectIdOrNull> for GitObjectId {
    type Error = UnexpectedNullGitObjectId;
    fn try_from(id: GitObjectIdOrNull) -> Result<Self, Self::Error> {
        if !id.chars().any(|c| c != '0') {
            Err(UnexpectedNullGitObjectId)
        } else {
            Ok(GitObjectId(id.into()))
        }
    }
}

impl TryFrom<String> for GitObjectId {
    type Error = InvalidOrNullGitObjectId;
    fn try_from(s: String) -> Result<Self, Self::Error> {
        let id: GitObjectIdOrNull = s.try_into()?;
        Ok(id.try_into()?)
    }
}

//---------- ForgeData ----------

#[derive(Deftly, Clone)]
#[derive_deftly(DebugTransparent, ToFromSqlInner)]
pub struct ForgeData(String);

impl ForgeData {
    pub fn from_raw_string(s: String) -> Self { ForgeData(s) }
    pub fn as_raw_string(&self) -> &str { &self.0 }
}

//---------- TagObjectData ----------

define_standard_string_newtype! {
    TagObjectData [
        error("syntactically invalid tag data");
    ]:

    // Allow any unicode characters!  We're just passing this about.
    fn char_ok(_c: char) -> bool { true }
}

//---------- ProcessingInfo ----------

#[derive(Error, Debug)]
#[error("trying to make empty ProcessingInfo")]
pub struct EmptyProcessingInfo;

define_standard_string_newtype! {
    ProcessingInfo []:

    type Error = EmptyProcessingInfo;

    fn char_ok(_: char) -> bool {
        // we trust ourselves
        true
    }

    fn map_err(_: AE) -> Self::Error { EmptyProcessingInfo }
}

impl From<EmptyProcessingInfo> for InternalError {
    #[track_caller]
    fn from(e: EmptyProcessingInfo) -> InternalError {
        IE::new(e.into())
    }
}

//---------- PackageName ----------

define_standard_string_newtype! {
    /// Debian Policy 6.5.1
    ///
    /// (Validation is complete according to that spec.)
    PackageName [
        error("syntactically invalid package name");
    ]:

    fn initial_char_ok(c: char) -> bool {
        c.is_ascii_alphanumeric()
    }
    fn char_ok(c: char) -> bool {
        c.is_ascii_lowercase()         ||
        c.is_ascii_digit()             ||
        "+-.".chars().any(|t| c == t)
    }
    fn other_checks(s: &str) -> Result<(), AE> {
        if s.len() < 2 { return Err(anyhow!("too short!")) }
        Ok(())
    }
}

//---------- VersionString ----------

define_standard_string_newtype! {
    /// Debian version string (Policy 5.6.12)
    ///
    /// We don't validate this completely,
    /// just enough to know it's not mad.
    VersionString [
        error("syntactically invalid package version string");
    ]:

    fn initial_char_ok(c: char) -> bool {
        c.is_ascii_digit()
    }
    fn char_ok(c: char) -> bool {
        c.is_ascii_alphanumeric()        || 
        ".+-~:".chars().any(|t| c == t)
    }
}

//---------- WorkerId ----------

define_standard_string_newtype! {
    WorkerId [
        error("syntactically invalid worker id");
    ]:

    fn initial_char_ok(c: char) -> bool {
        c.is_ascii_alphanumeric()
    }
    fn char_ok(c: char) -> bool {
        c.is_ascii_lowercase()         ||
        c.is_ascii_digit()             ||
        "-.,".chars().any(|t| c == t)
    }
}
