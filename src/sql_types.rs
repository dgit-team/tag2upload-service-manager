
use crate::prelude::*;

//---------- via String ----------

define_derive_deftly! {
    export SqlViaString expect items:

    impl<$tgens> rusqlite::types::FromSql for $ttype {
        fn column_result(value: rusqlite::types::ValueRef)
                         -> rusqlite::types::FromSqlResult<Self> {
            let s = <String as rusqlite::types::FromSql>::column_result(value)?;
            s.try_into().map_err(
                |e| rusqlite::types::FromSqlError::Other(Box::new(e))
            )
        }
    }

    impl<$tgens> ToSql for $ttype {
        fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput> {
            Ok(String::from(self.clone()).into())
        }
    }
}

//---------- inner String ----------

define_derive_deftly! {
    /// Does no checks on the data!
    export ToFromSqlInner expect items:

    impl ToSql for $ttype {
        fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput> {
            ToSql::to_sql(&self.0)
        }
    }

    impl FromSql for $ttype {
        fn column_result(value: rusqlite::types::ValueRef)
                         -> rusqlite::types::FromSqlResult<Self> {
            let s = FromSql::column_result(value)?;
            Ok($tname(s))
        }
    }
}

//---------- Enum ----------

#[derive(Error, Clone, Debug, Eq, PartialEq, Hash)]
#[error("invalid enum value {value:?} for {ty} found in database")]
pub struct FromSqlEnumInvalidValue {
    pub ty: String,
    pub value: String,
}

define_derive_deftly! {
    export ToSqlEnum for enum, expect items:

    impl<$tgens> ToSql for $ttype {
        fn to_sql(&self) -> rusqlite::Result<rusqlite::types::ToSqlOutput> {
            let s = match self {
              $(
                $vtype {} => stringify!($vname),
              )
            };
            s.to_sql()
        }
    }
}

define_derive_deftly! {
    export FromSqlEnum for enum, expect items:

    #[doc(hidden)]
    pub mod $<impl_fromsql_for ${snake_case $tname}> {
        use $crate::prelude::*;
        use $crate::sql_types::FromSqlEnumInvalidValue;

        use rusqlite::types::{FromSql, FromSqlError, FromSqlResult, ValueRef};

        impl FromSql for $ttype {
            fn column_result(value: ValueRef) -> FromSqlResult<Self> {
                let s = match value {
                    ValueRef::Text(s) => s,
                    _ => return Err(FromSqlError::InvalidType),
                };
              $(
                if s == stringify!($vname).as_bytes() {
                    Ok($vtype {})
                } else
              )
                {
                    Err(FromSqlError::Other(FromSqlEnumInvalidValue {
                        ty: stringify!($tname).to_owned(),
                        value: String::from_utf8_lossy(s).into_owned(),
                    }.into()))
                }
            }
        }
    }
}
