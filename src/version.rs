
use crate::prelude::*;

use std::hint::black_box;
use std::option_env;
use std::process::Command;

#[derive(Serialize, Debug, derive_more::Display)]
#[serde(rename_all = "snake_case")]
pub enum Info {
    #[display("debug build")]
    Debug,
    #[display("ad-hoc release build")]
    Adhoc,
    #[display("unattested release build")]
    Unattested,
    /// no `files.self_git_dir`, or `maint/build-repro describe` failed
    #[display("{raw_source}")]
    #[serde(untagged)]
    RawSource { raw_source: String },
    /// `maint/build-repro describe` succeeded
    #[display("{attested}")]
    #[serde(untagged)]
    Attested { raw_source: String, attested: String },
}

pub fn raw_info() -> Info {
    if cfg!(debug_assertions) { return Info::Debug }

    let Some(reference_commit) = option_env!("T2USM_REPRO_GIT_COMMIT")
    else { return Info::Adhoc };

    // Using `black_box` will hopefully limit the additional delta in
    // the dirty case.  See maint/build-repro.
    let reference_commit = black_box(reference_commit);

    if reference_commit == "0000000000000000000000000000000000000000"
    { return Info::Unattested }

    let raw_source = reference_commit.to_owned();

    return Info::RawSource { raw_source };
}

pub fn calculate_describe(config: &config::Files) -> Info {
    let info = raw_info();

    let Some(self_git_dir) = &config.self_git_dir
    else { return info };

    let Info::RawSource { raw_source } = &info
    else { return info };

    let attested = match (|| {

        let output = Command::new("bash")
            .env("GIT_DIR", self_git_dir)
            .args([
                "-c",
                // Using embedded copy makes us self-contained and avoids any
                // trouble with (possibly concurrent) udpates.
                include_str!("../maint/build-repro"),
                "maint/build-repro (embedded)",
                "describe",
                &raw_source,
            ])
            .output().context("spawn")?;

        if !(output.status.success() && output.stderr.is_empty()) {
            let stderr = String::from_utf8_lossy(&output.stderr);
            return Err(anyhow!(
                "failed: {} ({})",
                output.status, stderr.trim_end(),
            ));
        }

        let attested = str::from_utf8(&output.stdout)
            .context("output")?
            .trim_end();

        info!(raw_source, attested, "resolved self git commit");

        Ok(attested.to_owned())

    })() {
        Err(error) => {
            warn!(%error, "maint/build-repro describe (embedded)");
            return info;
        }
        Ok(y) => y,
    };

    let raw_source = raw_source.to_owned();

    return Info::Attested { raw_source, attested }
}
