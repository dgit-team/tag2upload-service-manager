//! support for defining routes - mostly, Tera templates
//!
//! We embed the templates in the binary because that's simple to deploy,
//! and avoids entanglement of the build system (and asset configuration).

use crate::prelude::*;

pub use tera::Tera;
pub use rocket::http::ContentType;

use rocket::request::{FromRequest, Outcome, Request};

pub type RenderedTemplate = Result<(ContentType, String), WebError>;

pub struct EmbeddedTemplateIsJustAPart;

pub struct EmbeddedTemplate {
    pub name: &'static str,
    pub contents: &'static str,
    pub is_part: Option<EmbeddedTemplateIsJustAPart>,
}

define_derive_deftly! {
    FromRequest for struct, expect items:

    #[async_trait]
    impl<'r> FromRequest<'r> for $ttype {
        type Error = String;

        async fn from_request(req: &'r Request<'_>) -> Outcome<Self, String> {
            use Outcome as O;

            O::Success($tname { $(
                $fname: match FromRequest::from_request(req).await {
                    O::Success(y) => y,
                    O::Forward(x) => return O::Forward(x),
                    O::Error(e)   => return O::Error(e),
                },
            ) })
        }
    }
}

#[derive(Deftly)]
#[derive_deftly(FromRequest)]
pub struct UiReqInfo {
    pub vhost: ui_vhost::UiResult,
    pub send_format: SendFormat,
}

pub enum SendFormat {
    Html,
    Json,
}

#[async_trait]
impl<'r> FromRequest<'r> for SendFormat {
    type Error = String;

    async fn from_request(req: &'r Request<'_>) -> Outcome<Self, String> {
        FromRequest::from_request(req).await.map(|a: &rocket::http::Accept| {
            if a.preferred().is_json() {
                SendFormat::Json
            } else {
                SendFormat::Html
            }
        }).map_error(|(_, inf)| match inf {})
    }
}

macro_rules! load_template_parts { { $( $name:literal ),* $(,)? } => { $(
    inventory::submit!(EmbeddedTemplate {
        name: $name,
        contents: include_str!(concat!("../ui/", $name)),
        is_part: Some(EmbeddedTemplateIsJustAPart),
    });
)* } }

/// Return a `Template` from a build-in template
///
// We can't test this because it would try to read some "FILENAME.html".
/// ```ignore
/// template! {
///     "NAME.html";
///   [ content_type: CONTENT_TYPE; ]
///     CONTEXT
/// }
/// ```
///
/// Expands to an expression returning a value of type [`RenderedTemplate`].
///
/// `NAME.html` corresponds to the file `ui/NAME.html`.
/// (It doesn't need to end in `.html`.
/// But, the extension influences tera's autoescaping.)
/// 
/// `{ CONTEXT_FIELD_NAME: CONTEXT_VALUE, .. }`
/// are values for the template.
/// Each `CONTEXT_VALUE` must be [`Serialize`].
///
/// `CONTEXT` is either `(EXPR)`, where `EXPR` is of type `tera::Context`,
/// or `{ CONTEXT_FIELD_NAME [: CONTEXT_VALUE ], .. }`
/// which is fed to `tera_context!`.
#[macro_export]
macro_rules! template { {
    $name:literal, $req_info:expr;
 $( content_type: $ctype:expr; )?
    { $($context_always:tt)* }
 $( { $($context_html_only:tt)* } )?
} => {
    $crate::template! {
        $name, $req_info;
     $( $content_type: $ctype )?
        ($crate::tera_context!( $( $context_always )* ),
         $crate::tera_context!( $( $( $context_html_only )* )? ))
    }
};
{
    $name:literal, $req_info:expr;
 $( content_type: $ctype:expr; )?
    ( $context_always:expr, $context_html_only:expr )
    $(,)?
} => {
    {
        use $crate::ui_render::*;

        let _: $crate::ui_vhost::IsUi =
            $req_info.vhost.check(WE::PageNotFoundHere)?;

        inventory::submit!(EmbeddedTemplate {
            name: $name,
            contents: include_str!(concat!("../ui/", $name)),
            is_part: None,
        });

        match $req_info.send_format {
            SendFormat::Html => {}
            SendFormat::Json => {
                let json = $context_always.into_json();
                let json = serde_json::to_string(&json)
                    .with_context(|| format!("render json for {}", $name))
                    .map_err(IE::new_quiet)
                    .map_err(WebError::from)?;
                return Ok((ContentType::JSON, json));
            }
        }
        let mut context = $context_always;
        context.extend($context_html_only);

        #[allow(dead_code)]
        let content_type = $name.rsplit_once('.')
            .and_then(|(_, ext)| ContentType::from_extension(ext));
      $(
        let content_type: ContentType = Some($ctype);
      )?
        let content_type = content_type
            .ok_or_else(|| WebError::from(IE::new_quiet(anyhow!(
                "cannot deduce Content-Type for template {}", $name
            ))))?;

        let gl = globals();

        #[cfg(test)]
        gl.t_note_template_rendered($name);

        let s = gl.tera.render(
            $name,
            &context,
        )
            .context($name)
            .map_err(IE::new_quiet)
            .map_err(WebError::from)?;

        Ok((content_type, s))
    }
} }

/// Invoke an HTML page template
///
/// Like `template!` but only takes the `{ }` context syntax,
/// and additionally includes a navbar as `navbar` in the context,
/// based on `$name` and
/// the prevailing constant `NAVBAR` (from the calling scope).
#[macro_export]
macro_rules! template_page { {
    $name:literal, $req_info:expr;
    { $($context:tt)* }
} => {
    $crate::template! {
        $name, $req_info;
        {
            $($context)*
            t2usm_version: &globals().version_info,
        }
        {
            navbar: $crate::ui_render::make_navbar_for($name, NAVBAR),
            t2usm_version: &globals().version_info.to_string(),
        }
    }
} }

/// Reimplementation, roughly, of `rocket_dyn_templates::context!`
///
/// ```
/// # use tag2upload_service_manager::tera_context;
/// # let VALUE = "12";
/// # let NAME_SAME_AS_VALUE = "42";
/// let _: tera::Context = tera_context! {
///     NAME: VALUE,
///     NAME_SAME_AS_VALUE,
/// };
/// ```
#[macro_export]
macro_rules! tera_context { {
    $( $k:ident $( : $v:expr )? ),* $(,)?
} => { {
    #[allow(unused_mut)]
    let mut context = tera::Context::new();
    $( $crate::tera_context!( @ context $k $( $v )? ); )*
    context
} }; {
    @ $context:ident $k:ident
} => {
    $crate::tera_context!(@ $context $k $k)
}; {
    @ $context:ident $k:ident $v:expr
} => {
    $context.insert(stringify!($k), &$v)
} }

pub fn tera_templates(config: &Config) -> Result<Tera, StartupError> {
    if let Some(dir) = &config.files.template_dir {
        let glob = format!("{dir}/*[^~#]");
        debug!(?glob, "loading tera templates");
        Tera::new(&glob)
            .context(glob)
            .map_err(StartupError::Templates)
    } else {
        embedded_tera_templates()
    }
}

pub fn embedded_tera_templates() -> Result<Tera, StartupError> {
    let mut tera = Tera::default();

    tera.add_raw_templates(
        inventory::iter::<EmbeddedTemplate>().map(
            |EmbeddedTemplate { name, contents, is_part: _ }| {
                trace!(name, "loading builtin templat");
                (name, contents)
            }
        )
    ).into_internal("failed to initialise templating")?;

    Ok(tera)
}

/// title text; template name; route path
pub type NavbarEntry<'s> = (&'s str, &'s str, &'s str);

pub fn make_navbar_for(for_templ: &str, navbar: &[NavbarEntry]) -> String {
    (|| {
        let mut out = String::new();
        let mut delim = "";
        for (title, templ, path) in navbar.iter().copied() {
            write!(out, "{}", mem::replace(&mut delim, " | "))?;
            let post = if templ != for_templ {
                write!(out, "<a href={path}>")?;
                "</a>"
            } else {
                ""
            };
            write!(out, "<b>{title}</b>{post}")?;
        }
        Ok::<_, fmt::Error>(out)
    })().expect("write to strings failed")
}

inventory::collect!(EmbeddedTemplate);

#[test]
fn check_embedded_tera_templates() {
    let t: Tera = embedded_tera_templates().expect("bad templates?");
    println!("{t:?}");
}
