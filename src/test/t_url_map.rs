//! URL mapping for tests, when we make requests
//!
//! See also `config::Testing.fake_https_dir`
//!
//! That can affect incoming repo url parsing, but necessarily
//! redirects only to files.

use super::*;

#[derive(Debug)]
pub(super) struct UrlMap {
    map: Mutex<Vec<(String, String)>>,
    dummy_url: Url,
}

impl Default for UrlMap {
    fn default() -> Self {
        UrlMap {
            map: vec![].into(),
            dummy_url: Url::parse("file:///dev/enoent").unwrap(),
        }
    }
}

pub trait UrlMappable: Display {
    type Output: FromStr<Err: Display> + From<Url>;
    fn map(&self) -> Self::Output {
        let url = self.to_string();
        let globals = globals();
        let url_map = &globals.test_suppl.url_map;
        (|| {
            let url_map = &url_map.map.lock().unwrap();
            let mapped = url_map
                .iter()
                .rev() // look for ones pushed last, first
                .find_map(|(pfx, replace)| {
                    let rhs = url.strip_prefix(pfx)?;
                    Some(format!("{replace}{rhs}"))
                })
                .ok_or_else(|| anyhow!(
                    "testing, no mapping"
                ))?;
            let mapped = mapped.parse()
                .map_err(|e| anyhow!("parse mapped url: {e}"))?;
            Ok::<_, AE>(mapped)
        })()
            .context(url)
            .unwrap_or_else(|ae| {
                IE::new(ae).note_only();
                url_map.dummy_url.clone().into()
            })
    }
}

impl UrlMappable for Url { type Output = Url; }
impl<'s> UrlMappable for &str { type Output = String; }

impl TestCtx {
    pub(super) fn map_url(
        &self,
        prefix: impl Display,
        replacement: impl Display,
    ) {
        self.gl.test_suppl.url_map.map.lock().unwrap().push((
            prefix.to_string(),
            replacement.to_string(),
        ));
    }
}
