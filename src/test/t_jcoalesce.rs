//! Test coalescence and blocking of related jobs

use super::*;

//---------- objectid and forge host for these tests ----------

fn test_objectid(which: u32) -> GitObjectId {
    format!("12345678000000000000000000000000{which:08x}")
        .parse().unwrap()
}
/// Selector for a git objectid
///
/// We can't have consts that are GitObjectId becauee it contains String.
/// This is roughly the least ugly at the use sites.
type GitObjectIdSelector = fn() -> GitObjectId;
const MAIN_OBJECTID:  GitObjectIdSelector = || test_objectid(0);
const OTHER_OBJECTID: GitObjectIdSelector = || test_objectid(1);

const OTHER_HOST: &str = "file:///dev/enoent/dummy-repo/other-forge";

//---------- general utilities ----------

async fn settle() {
    const SETTLE_DURATION: Duration = Duration::from_secs(1);

    // This is a bodge.  Ideally we'd just wait for every task to do its work,
    // bot tokio's yield doesn't guarantee that.
    tokio::time::sleep(SETTLE_DURATION).await;
}

async fn expect_fetching(block: &BlockHook, jids: &[JobId]) {
    settle().await;

    let exp = jids.iter()
        .map(|jid| format!("fetch gitlab {jid}"))
        .collect::<HashSet<_>>();
    assert_eq!(globals().points_blocked_by(block), exp);
}

fn get_job_row(jid: JobId) -> TestResult<JobRow> {
    let jr = db_transaction(TN::Readonly, |dbt| {
        dbt.bsql_query_1(bsql!(
            "SELECT * FROM jobs WHERE jid = " jid ""
        ))
    })??;
    Ok(jr)
}

//---------- inserting test data ----------

/// Insert a test job, in some state (general case)
///
/// We insert things directly in the db, rather than messing about
/// with webhooks.  That's simpler and gives us more control over the
/// db contents.
fn insert_job_any(
    tc: &TestCtx, what: &str,
    tag_objectid: GitObjectIdSelector, repo_git_url: &str,
    forge_host: &str, status: JobStatus, tag_data: Option<TagObjectData>,
) -> TestResult<JobId> {
    let tag_objectid = tag_objectid();
    let now = tc.gl.now();

    let tag_meta = t2umeta::Parsed {
        source: "unused".parse()?,
        version: "0.version".parse()?,
    };

    let forge_data = GL_PROJECT.to_string();

    let data = JobData {
        repo_git_url: repo_git_url.to_owned(),
        tag_objectid: tag_objectid.clone(),
        tag_name: format!("irrelevant ({what})"),
        forge_host: forge_host.parse()?,
        forge_namever: gitlab::Forge1.namever_str().parse()?,
        forge_data: ForgeData::from_raw_string(forge_data),
        tag_meta,
    };

    let row = JobRow {
        jid: JobId::none(),
        received: now,
        last_update: now,
        tag_data: tag_data.into(),
        status,
        processing: None.into(),
        info: format!("job inserted by hand in test case ({what})"),
        duplicate_of: None,
        data,
    };

    let jid = db_transaction(TN::Update {
        this_jid: None,
        tag_objectid: &tag_objectid,
    }, |dbt| {
        dbt.bsql_insert(bsql!("INSERT INTO jobs " +~(row) ""))
    })??;

    let jid = rusqlite::types::ValueRef::Integer(jid);
    let jid = FromSql::column_result(jid)?;

    debug!(%jid, %what, %tag_objectid, %repo_git_url, "inserted");

    Ok(jid)
}

/// Insert a test job at the main forge host, `Noticed` (as if from webhook)
fn insert_job(
    tc: &TestCtx, what: &str,
    tag_objectid: GitObjectIdSelector, repo_git_url: &str,
) -> TestResult<JobId> {
    insert_job_any(
        tc, what,
        tag_objectid, repo_git_url,
        FORGE_HOST, JobStatus::Noticed, None,
    )
}

//---------- Test db_data.rs head comment Coalescing point 1 ----------

test_case! {
    fn not_simultaneous(temp_dir, abort):

    let tc = t_start(&temp_dir, abort, json!({})).await?;

    let block_fetches = tc.gl.t_block("fetch gitlab *");

    let j_base = insert_job(&tc, "base", MAIN_OBJECTID,  FORGE_HOST)?;
    let _jvar1 = insert_job(&tc, "var1", MAIN_OBJECTID,  OTHER_HOST)?;
    let _jvar2 = insert_job(&tc, "var2", OTHER_OBJECTID, FORGE_HOST)?;
    let j_diff = insert_job(&tc, "diff", OTHER_OBJECTID, OTHER_HOST)?;

    expect_fetching(&block_fetches, &[j_base, j_diff]).await;

    Ok(())
}

//---------- Test db_data.rs head comment Coalescing point 1 ----------

/// Test with "other job" state `already_st`
async fn case_pause_fetch_general(
    temp_dir: Arc<TestTempDir>,
    abort: AbortHandle,
    other_st: JobStatus,
) -> TestResult<()> {
    let tc = t_start(&temp_dir, abort, json!({})).await?;

    let block_fetches = tc.gl.t_block("fetch gitlab *");

    // insert the "other" job
    let j_other = insert_job_any(
        &tc, "other",
        MAIN_OBJECTID, OTHER_HOST,
        "other-job-forge-host", other_st,
        Some(format!("dummy tag data for other job").parse()?),
    )?;

    // The other job isn't supposed to run because it has a forge
    // host for which we have no task.  (The webhook wouldn't let
    // anyone create such a thing, because it checks.)
    expect_fetching(&block_fetches, &[]).await;

    // insert "this" job
    let j_this = insert_job(&tc, "new", MAIN_OBJECTID, FORGE_HOST)?;

    let expect_fetching = |l| expect_fetching(&block_fetches, l);

    use JobStatus as J;

    match other_st {
        J::Noticed | J::Queued | J::Building => {
            expect_fetching(&[]).await;
            let jr_this = get_job_row(j_this)?;
            assert_eq!(*jr_this.processing, None);
        }
        J::Irrecoverable | J::Uploaded => {
            expect_fetching(&[]).await;
            let jr_this = get_job_row(j_this)?;
            assert_eq!(jr_this.duplicate_of, Some(j_other));
        }
        J::Failed | J::NotForUs => {
            expect_fetching(&[j_this]).await;
        }
    };

    Ok(())
}

// Make a test case for each variant in JobStatus

derive_deftly_adhoc! {
    db_data::JobStatus:

  $(
    test_case! {
        fn $< pause_fetch_ ${snake_case $vname} >(temp_dir, abort):
        case_pause_fetch_general(temp_dir, abort, $vtype).await
    }
  )
}
