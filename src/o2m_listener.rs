
use crate::prelude::*;
use tokio::net::UnixListener;

use crate::o2m_support::OracleConnection;
use crate::o2m_messages as m;

#[allow(unused)] // Used during transitions, when we support several
use m::ProtocolVersion::*;

use db_workflow::Update;
use db_data::JobStatus as JS;

const MAX_CONSECUTIVE_ACCEPT_ERRORS: u32 = 100;

#[derive(Debug)]
pub struct Listener {
    s: UnixListener,
    globals: Arc<Globals>,
}

impl Listener {
    pub fn new(globals: &Arc<Globals>) -> Result<Self, StartupError> {
        let path = &globals.config.files.o2m_socket;
        (|| {
            fs::remove_file(path)
                .or_else(|e| {
                    use io::ErrorKind as EK;
                    match e.kind() {
                        EK::NotFound => Ok(()),
                        _ => Err(e),
                    }
                })
                .context("remove old socket")?;
            let s = UnixListener::bind(path).context("set up new socket")?;
            let globals = globals.clone();
            debug!(?path, "listening");

            if let Some(interval) =
                globals.config.timeouts.socket_stat_interval
            {
                globals.spawn_task_immediate("o2m stat", {
                    let path = path.clone();
                    async move {
                        loop {
                            let _: fs::Metadata = fs::metadata(&path)
                                .unwrap_or_else(|e| {
                                    error!("socket {path:?} gone? {e}");
                                    std::process::exit(127);
                                });
                            tokio::time::sleep(*interval).await;
                        }
                    }
                });
            }

            Ok::<_, AE>(Listener { s, globals })
        })()
            .with_context(|| format!("path {path:?}"))
            .map_err(StartupError::WorkerListener)
    }

    pub fn start_task(self) {
        self.globals.clone().spawn_task_running(
            format!("o2m listener"),
            self.listen_task(),
        )
    }

    pub async fn listen_task(self) -> TaskResult {
        let mut error_count = 0;
        let mut seq = 0;
        loop {
            match select! {
                biased;
                sd = self.globals.await_shutdown() => return Err(sd)?,
                sa = self.s.accept() => sa,
            } {
                Ok((stream, _addr)) => {
                    error_count = 0;
                    seq += 1;
                    self.globals.spawn_task_running(
                        "o2m connection",
                        connection_task(stream, seq),
                    );
                },
                Err(error) => {
                    error!("error accepting: {error}");
                    error_count += 1;
                    if error_count > MAX_CONSECUTIVE_ACCEPT_ERRORS {
                        Err(
                            internal!("persistent accept failures: {error}")
                        )?;
                    }
                }
            }
        }
    }
}

async fn connection_task(conn: UnixStream, seq: u64) -> TaskResult {
    let mut conn = OracleConnection::from_unix_socket(conn, seq)?;

    let error = conversation(&mut conn).await.void_unwrap_err();

    Ok(match error {
        OTE::Disconnected |
        OTE::Io(_) |
        OTE::Shutdown(_) |
        OTE::InternalError(_) => {
            debug!(%conn, %error, "ended");
            TaskWorkComplete {}
        },

        message @ (
            OTE::PeerReportedProtocolViolation(_) |
            OTE::UnsupportedVersion(_)
        ) => {
            error!(%conn, %message, "protocol error");
            TaskWorkComplete {}
        },

        error @ (
            OTE::BadMessage(_) |
            OTE::MaxLineLengthExceeded
        )=> {
            error!(%conn, %error, "protocol error");

            let msg = m::ProtocolViolation {
                message: error.to_string()
            };
            match conn.xmit(&msg).await {
                Ok(()) => {},
                Err(error) => trace!(%conn, %error, "xmit violation report"),
            };
            TaskWorkComplete {}
        },
    })
}

async fn ayt_check(
    conn: &mut OracleConnection,
) -> Result<(), OracleTaskError> {
    conn.xmit(&m::Ayt {}).await?;
    let m::Ack {} = conn.recv().await?;
    Ok(())
}

async fn conversation(
    conn: &mut OracleConnection,
) -> Result<Void, OracleTaskError> {
    use WorkerPhase as WP;

    debug!(%conn, "accepted");

    conn.xmit(&m::Ready {}).await?;

    let v: m::VersionRequest = conn.recv().await?;

    #[allow(unused)] // Used during transitions, when we support several
    let v = m::ProtocolVersion::from_repr(
        v.version.try_into().expect("u32 didn't fit in usize, 16-bit machine!")
    )
        .ok_or_else(|| OTE::UnsupportedVersion(v))?;

    let m::WorkerId { worker } = conn.recv().await?;

    conn.set_worker_id(worker.clone())?;

    loop {
        ayt_check(conn).await?;

        conn.update_report(WP::Idle, None, |_wr| {})?;

        let log_xinfo = conn.to_string();
        let update = Update {
            new_status: None,
            new_info: &format!("selected for build [{worker}]"),
        };

        let mut job = select! {
            biased;
            m = conn.recv::<Void>() => void::unreachable(m?),
            job = JobInWorkflow::start(
                "build", log_xinfo,
                JS::Queued,
                bsql!("TRUE"),
                &update,
            ) => job.map_err(|qt| match qt {
                QuitTask::Shutdown(sd) => OTE::Shutdown(sd),
                QuitTask::Crashed(ie) => OTE::InternalError(ie),
            })?,
        };

        conn.update_report(WP::Selected, Some(&job), |_wr| {})?;

        ayt_check(conn).await?;

        conn.update_report(WP::Building, Some(&job), |_wr| {})?;

        job.update(
            &Update {
                new_status: Some(JS::Building),
                new_info: &format!("building [{worker}]"),
            },
            &JobRowUpdate::default(),
        )?;

        conn.update_report(WP::Building, Some(&job), |_wr| {})?;

        let job = IrrecoverableIfDropped::new(job);

        let outcome = async {
            let body = job.tag_data.0.clone()
                .ok_or_else(|| internal!("JS::Queued but no tag data"))?;

            let url = job.data.repo_git_url.clone();
            let jid = job.jid;
            conn.xmit(&m::Job { url, jid, body }).await?;

            let m::Message { message } = conn.recv().await?;
            let outcome: m::Outcome = conn.recv().await?;

            let info = format!(
                "{} [{worker}]: {}",
                match outcome {
                    m::Outcome::Uploaded(..) => "uploaded",
                    m::Outcome::Irrecoverable(..) => "failed",
                },
                message
            );

            let status = outcome.job_status();

            Ok::<_, OracleTaskError>(Outcome { status, info })
        }.await.unwrap_or_else(|e| Outcome {
            status: JS::Irrecoverable,
            info: format!("error while awaiting build [{worker}]: {e}"),
        });

        conn.update_report(WP::Idle, None, |wr| {
            outcome.update_worker_report(wr);
        })?;

        job.outcome(outcome)?;
    }
}

#[derive(Deftly)]
#[derive_deftly(UpdateWorkerReport)]
pub struct Outcome {
    #[deftly(worker_report)]
    pub status: JobStatus,
    #[deftly(worker_report)]
    pub info: String,
}

struct IrrecoverableIfDropped(Option<JobInWorkflow>);

impl Deref for IrrecoverableIfDropped {
    type Target = JobInWorkflow;
    fn deref(&self) -> &JobInWorkflow {
        self.0.as_ref().expect("None!")
    }
}

impl Drop for IrrecoverableIfDropped {
    fn drop(&mut self) {
        if let Some(job) = self.0.take() {
            Self::outcome_inner(job, Outcome {
                status: JS::Irrecoverable,
                info: format!("Rust async task dropped while building?!"),
            }).context(
                "Irrecoverable due to drop"
            ).unwrap_or_else(|e| { IE::new(e); })
        }
    }
}

impl IrrecoverableIfDropped {
    fn new(job: JobInWorkflow) -> Self {
        IrrecoverableIfDropped(Some(job))
    }

    fn outcome(mut self, outcome: Outcome) -> Result<(), OracleTaskError> {
        let job = self.0.take().expect("taken already?");
        Self::outcome_inner(job, outcome)
    }

    fn outcome_inner(
        mut job: JobInWorkflow,
        outcome: Outcome,
    ) -> Result<(), OracleTaskError> {
        let Outcome { status, info } = outcome;
        job.update(
            &Update {
                new_status: Some(status),
                new_info: &info,
            },
            &JobRowUpdate::default(),
        )?;
        Ok(())
    }
}
