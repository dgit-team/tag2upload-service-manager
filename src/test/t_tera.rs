
use super::*;

#[derive(Debug, Default)]
pub struct TemplatesRenderedRecently(Mutex<Vec<String>>);

#[derive(Debug, Default)]
pub struct TemplatesRendered(HashMap<String, u32>);

impl Globals {
    pub fn t_note_template_rendered(&self, name: &str) {
        self.test_suppl.templates_rendered_recently.0
            .lock().unwrap()
            .push(name.to_owned());
    }
}

impl TestCtx {
    pub(super) fn rendered_page_filename(&self, name: &str, count: u32) -> String {
        let (stem, ext) = name.split_at(
            name.rfind('.').unwrap_or(name.len())
        );
        format!("{}/rendered/{stem}-{count}{ext}", self.temp_dir())
    }

    pub(super) async fn load_page(
        &mut self,
        path: &str,
    ) -> TestResult<String> {
        let body = reqwest::Client::new()
            .get(format!("http://127.0.0.1:{}{path}", self.port))
            .header("Host", VHOST_UI)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;
        debug!("{path:?} => {body:?}");
        assert!(! body.is_empty());

        let name = self.gl.test_suppl.templates_rendered_recently.0
            .lock().unwrap()
            .drain(..)
            .exactly_one()?;

        {
            // If there is a navbar entry matching the path or the template
            // it should match both.
            //
            // (Missing navbar entries are not detected by the tests.
            // Extra navbar entries *are*, at the end>0
            for (_, nb_templ, nb_path) in ui_routes::NAVBAR {
                assert_eq!(
                    nb_templ == &name,
                    *nb_path == path,
                    "this {name:?} {path:?} navbar {nb_templ:?} {nb_path:?}",
                );
            }
        }

        let count = {
            let c = self.templates_rendered.0.entry(name.clone()).or_default();
            let r = *c;
            *c += 1;
            r
        };

        {
            fs::write(
                self.rendered_page_filename(&name, count),
                &body,
            )?;
        }

        Ok(body)
    }
}

pub(super) fn t_check_all_templates_tested(tc: &TestCtx) -> TestResult<()> {
    let recently = tc.gl.test_suppl.templates_rendered_recently.0
        .lock().unwrap();
    assert!(recently.is_empty(), "{recently:?}");

    let rendered = &tc.templates_rendered;

    let untested = inventory::iter::<ui_render::EmbeddedTemplate>()
        .filter(|et| et.is_part.is_none())
        .map(|et| et.name)
        .filter(|name| !rendered.0.contains_key(*name))
        .collect_vec();

    assert!(untested.is_empty(), "OK={rendered:#?} missed={untested:#?}");

    let navbar_unused = ui_routes::NAVBAR.iter().copied()
        .map(|(_, templ, _)| templ)
        .filter(|name| !rendered.0.contains_key(*name))
        .collect_vec();

    assert!(navbar_unused.is_empty(), "{navbar_unused:#?}");

    Ok(())
}

pub(super) fn t_run_weblint(tc: &TestCtx) -> TestResult<()> {
    let mut cmd = std::process::Command::new("weblint");
    cmd.args(["--context", "--"]);
    for (name, count) in &tc.templates_rendered.0 {
        if name.ends_with(".html") {
            for count in 0..*count {
                cmd.arg(tc.rendered_page_filename(name, count));
            }
        }
    }
    let stdout = cmd.stdout_checked()?;

    if !(stdout.is_empty()) {
        eprintln!("***** weblint *****\n{stdout}\n***** weblint *****");
        panic!("weblint detected errors\n\n");
    }

    Ok(())
}
